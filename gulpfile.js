const gulp = require("gulp");
const browserSync = require("browser-sync").create();
const webpack = require("webpack");
const fs = require("fs");
const path = require("path");
const browserify = require("browserify");
const source = require("vinyl-source-stream");
const tsify = require("tsify");
var exec = require("child_process").exec;
const getDirectories = source => {
    return fs.readdirSync(source, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => path.join(source, dirent.name));
}
gulp.task("build-examples", (cb) => {
    return new Promise((resolve, reject) => {
        // locate all directories with main.js and index.html
        let examplesDir = path.resolve("./examples");
        let unvisited = getDirectories(examplesDir);
        let buildDirs = [];
        while (unvisited.length != 0) {
            let currentDir = unvisited.shift();
            if (fs.existsSync(currentDir + "/main.js") && fs.existsSync(currentDir + "/index.html")) {
                buildDirs.push(currentDir);
                continue;
            }
            unvisited.push(...getDirectories(currentDir));
        }
        console.log("BuildDirs are", buildDirs);
        let buildCompletedCount = 0;
        function buildES6(contextDir, destdDir) {
            webpack({
                entry: path.resolve(path.join(contextDir, "main.js")),
                output: {
                    filename: "dist.js",
                    path: path.join(destdDir, "dist")
                },
                module: {
                    rules: [
                        {
                            test: /\.js$/,
                            loader: 'babel-loader',
                            exclude: /node_modules/,
                            query: {
                                presets: ["@babel/preset-env"]
                            }
                        }
                    ]
                }
            }, (err, stats) => {
                if (err) {
                    console.log(err);
                }
                if (stats.hasErrors()) {
                    return reject(new Error(stats.compilation.errors.join('\n')))
                }
                buildCompletedCount++;
                if (buildCompletedCount == buildDirs.length)
                    resolve();
            })
        }
        for (let i = 0; i < buildDirs.length; i++) {
            buildES6(buildDirs[i], buildDirs[i]);
        }
    });
})

gulp.task("serve", (cb) => {
    browserSync.init({
        server: "./",
        port: 4444,
        host: "127.0.0.1"
    }, cb);
});

gulp.task("reload", (cb) => {
    browserSync.reload();
    cb();
});

gulp.task("watch", (cb) => {
    return gulp.watch(
        "**/*",
        {
            ignored: ["dist/**/*", "**/*/dist"]
        },
        gulp.series("build-examples", "reload")
    );
});

gulp.task("build-es5", function () {
    return browserify({
        basedir: ".",
        debug: true,
        entries: ["src/public.ts"],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .bundle()
        .pipe(source("bundle.js"))
        .pipe(gulp.dest("dist/es5"));
});

gulp.task("build-es2015", (cb) => {

});

gulp.task("publish-es5", gulp.series(
    function(cb){
        return gulp.src("package.json").pipe(gulp.dest("dist/es5"));
    },
    function(cb){
        let promise = new Promise(function (resolve, reject) {
            let childProcess = exec("cd dist/es5 && npm publish --access public", (error, stdout, stderr) => {
              if (error) {
                console.log(`error: ${error.message}`);
                return;
              }
              if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
              }
              console.log(`stdout: ${stdout}`);
            });
            childProcess.on("exit", function (code) {
              console.log("code is", code);
              if (code != 0) {
                reject("npm publish exited with non zero code");
                return;
              }
              resolve("Web package published successfully");
            });
          });
          return promise;
    }
));