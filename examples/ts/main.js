let page1 = document.getElementById("page1");
let appConfig = new window.Art.ActivityConfig();
appConfig.shapeCSS = { fill: "rgba(255, 0, 0, 0.2)" }
let activity = new window.Art.Activity(page1, 0, appConfig);
console.log("activity imported", activity);
activity.run();

function createActivity(event) {
    let page1 = document.getElementById("page1");
    let appConfig = new window.Art.ActivityConfig();
    appConfig.shapeCSS = { fill: "rgba(255, 0, 0, 0.2)" }
    let activity = new window.Art.Activity(page1, 0, appConfig);
    console.log("activity imported", activity);
    activity.run();
}

window.addEventListener("cruxdragcomplete", (ev) => {
    console.log(ev);
    console.log(ev.detail.activity.getClientPosition());
});

window.addEventListener("cruxactivitycomplete", (ev)=>{
    console.log(ev);
    console.log("after active complete", ev.detail.activity.getClientPosition());
});