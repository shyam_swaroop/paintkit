var isMousePressed = false;
var cEl;
var cx, cy;
var cH; // current handle
function getDelta(event){
    let delta = {
        dx: event.clientX - cx,
        dy: event.clientY - cy
    }
    cx = event.clientX;
    cy = event.clientY;
    return delta;
}

function onMouseDown(event){
    isMousePressed = true;
    event.stopPropagation();
    cEl =  event.target.parentElement;
    cx = event.clientX;
    cy = event.clientY;
    let delta = getDelta(event);
    let svgWidth = parseInt(cEl.querySelector("svg").getAttribute("width"));
    let childWidth = parseInt(cEl.querySelector("svg:first-child").getAttribute("width"));
    console.log("onMouseDown",  svgWidth, childWidth);
    cEl.querySelector("svg").setAttribute("width", svgWidth + delta.dx);
    cEl.querySelector("svg:first-child").setAttribute("width", childWidth + delta.dx);
    window.addEventListener("mousemove", onMouseMove);
    window.addEventListener("mouseup", onMouseUp);
}

function onMouseMove(event){
    console.log("onMouseMove called");
    if(!isMousePressed){
        return;
    }
    let delta = getDelta(event);
    let svgWidth = parseInt(cEl.querySelector("svg").getAttribute("width"));
    let childWidth = parseInt(cEl.querySelector("svg > rect").getAttribute("width"));
    console.log("onMouseMove", svgWidth, childWidth, delta.dx);
    cEl.querySelector("svg").setAttribute("width", svgWidth + delta.dx);
    cEl.querySelector("svg > rect").setAttribute("width", childWidth + delta.dx);
    cH.style.right = parseInt(cH.style.right.replace("px", "")) - delta.dx + "px";
    // console.log("cH", cH)
}

function onMouseUp(event){
    console.log("onMouseUp called")
    isMousePressed = false;
    window.removeEventListener("mouseup", this);
    window.removeEventListener("mousemove", onMouseMove);
}

function createRightHandle(el){
    console.log("createRightHandle called");
    let rightHandle = document.createElement("div");
    rightHandle.style.width = "10px";
    rightHandle.style.height = el.style.height;
    rightHandle.style.position = "absolute";
    rightHandle.style.right = "0";
    rightHandle.style.top = "0";
    rightHandle.style.background = "blue";
    rightHandle.classList.add("righthandle");
    el.appendChild(rightHandle);
    rightHandle.addEventListener("mousedown", onMouseDown);
    // rightHandle.addEventListener("mousemove", onMouseMove);
    return rightHandle;
}

function destroyRightHandle(el){
    if(el.querySelector(".righthandle"))
    el.querySelector(".righthandle").remove();
}

export function makeResizable(el){
    console.log("makeResizable called", el);
    // return if already resizable
    if(el.classList.contains("resizable")){
        return;
    }
    cEl = el;
    el.classList.add("resizable");
    cH = createRightHandle(el);
    console.log("makeResizable done");
}

export function makeUnResizable(el){
    destroyRightHandle(el);
}