import { makeDraggable } from './draggable';
import { makeResizable, makeUnResizable } from './resizable';
var isMousePressed = false;
let coord = { x: 0, y: 0 };    // this variable stores initial mouse co-ordinates
let currentDiv;
let currentSvg;
let currentRect;
let minX;
let maxX;
let minY;
let maxY;
let startX;
let startY;
let ShapeType = {
    UNK: 0,
    FREE: 1,
    RECT: 2,
    ELLIPSE: 3,
    CIRCLE: 4
};
let currentShape = ShapeType.UNK;
console.log("current shape is", currentShape);
var radios = document.querySelectorAll('input[type=radio][name="shape"]');
var currentSelection;   // stores current selected element
function changeHandler(event) {
    if (this.value === '1') {
        console.log('value', '1');
        currentShape = ShapeType.FREE;
    } else if (this.value === '2') {
        console.log('value', '2');
        currentShape = ShapeType.RECT;
    } else if (this.value === '3') {
        console.log('value', '3');
        currentShape = ShapeType.ELLIPSE;
    } else if (this.value === '4') {
        console.log('value', '4');
        currentShape = ShapeType.CIRCLE;
    }
    console.log("current shape is", currentShape);
}

Array.prototype.forEach.call(radios, function (radio) {
    radio.addEventListener('change', changeHandler);
});

function getPageDiv(pageNumber) {
    return document.querySelector('div[data-page-number="' + pageNumber + '"]');
}

function setPageDivProperties(page, width, height) {
    page.style.height = height;
    page.style.width = width;
    SVG(page);
}

function setPageDivSvgEventListeners(page) {
    page.querySelector("svg").addEventListener("click", (event) => {
        console.log("clicked");

    });
    page.querySelector("svg").addEventListener("mousedown", startPainting);
    page.querySelector("svg").addEventListener("mouseup", stopPainting);
    page.querySelector("svg").addEventListener("mousemove", paint);
    // page.querySelector("svg").addEventListener("mouseout", stopPainting);  // stop painting if mouse leaves the div
}

function updatePosition(event) {
    coord.x = event.clientX - currentDiv.getBoundingClientRect().left;
    coord.y = event.clientY - currentDiv.getBoundingClientRect().top;
}

function startPainting(event) {
    if (isMousePressed) {
        console.log("WARN: attempting to invoke a new paint session while their is another active paint session");
        return;
    }
    isMousePressed = true;
    currentDiv = event.target;
    updatePosition(event);
    startX = coord.x;
    startY = coord.y;
    minX = coord.x;
    maxX = coord.x;
    minY = coord.y;
    maxY = coord.y;
    if (currentShape != undefined && currentShape != ShapeType.UNK) {
        console.log("checked passed", event.target);
        currentSvg = event.target;
        drawRect();
    } else {
        return;
    }
}

// function getPageSVG(page){
//     return page.querySelector("svg");
// }

function stopPainting(event) {
    if (!isMousePressed) {
        console.log("INFO: attempting to stop a paint session, while there are no acive paint session");
        return;
    }
    isMousePressed = false;

    if (currentShape == ShapeType.RECT && currentSvg && currentRect) {
        // transform and place the svg apt
        console.log("transforming svg and rect");
        let gap = 10;
        let newX = currentRect.getAttribute('x') - gap;
        let newY = currentRect.getAttribute('y') - gap;
        let newWidth = parseInt(currentRect.getAttribute('width').replace("px", "")) + 2 * gap + "px";
        let newHeight = parseInt(currentRect.getAttribute('height').replace("px", "")) + 2 * gap + "px";
        currentSvg.parentElement.style.top = newY + "px";
        currentSvg.parentElement.style.left = newX + "px";
        currentSvg.parentElement.style.height = newHeight;
        currentSvg.parentElement.style.width = newWidth;
        currentSvg.parentElement.style.display = "inline-block";
        currentSvg.setAttribute('width', newWidth);
        currentSvg.setAttribute('height', newHeight);
        console.log("new svg attributes", newX, newY, newWidth, newHeight);
        let newRectX = gap;
        let newRectY = gap;
        currentRect.setAttribute('x', newRectX);
        currentRect.setAttribute('y', newRectY);
        console.log("new rect attributes", newRectX, newRectY);
        // remove all event listeners from svg
        let cloneSvg = currentSvg.cloneNode(true);
        currentSvg.parentNode.replaceChild(cloneSvg, currentSvg);
        // give new event listeners to the svg
        cloneSvg.addEventListener("click", (event)=>{
            console.log("cloned svg clicked");
            event.stopPropagation();
            makeResizable(event.target.parentElement.parentElement);
            currentSelection = event.target.parentElement.parentElement;
        });
        makeDraggable(cloneSvg.parentElement);
    }


    currentSvg = undefined;
    currentRect = undefined;
    console.log("Bounding Box of object", minX, maxX, minY, maxY);
}

window.addEventListener("click", ()=>{
    if(currentSelection){
        makeUnResizable(currentSelection);
    }
});
function paint(event) {
    if (!isMousePressed) return;

    if (currentShape == ShapeType.FREE) {
        drawFree(event);
        // console.log(coord.x, coord.y);
        if (coord.x > maxX) {
            maxX = coord.x;
        }
        if (coord.x < minX) {
            minX = coord.x;
        }
        if (coord.y > maxY) {
            maxY = coord.y;
        }
        if (coord.y < minY) {
            minY = coord.y;
        }
    }

    if (currentShape == ShapeType.RECT) {
        updatePosition(event);
        renderRect(event);
        // console.log(coord.x, coord.y);
        minX = startX;
        minY = startY;
        maxX = coord.x;
        maxY = coord.y;
    }


}

function SVG(page) {
    console.log("SVG called")
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute('style', 'border: 1px solid black');
    svg.setAttribute('width', '1000');
    svg.setAttribute('height', '500');
    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
    var parentDiv = document.createElement("div");
    parentDiv.style.width = page.getAttribute("width");
    parentDiv.style.height = page.getAttribute("height");
    parentDiv.style.position = "relative";
    page.appendChild(parentDiv);
    parentDiv.appendChild(svg);
    return svg;
}

function drawFree(event) {

}

function drawRect() {
    if (!isMousePressed || !currentSvg) {
        return;
    }
    console.log("drawRect called");
    var svgns = "http://www.w3.org/2000/svg";
    var rect = document.createElementNS(svgns, 'rect');
    rect.setAttributeNS(null, 'x', startX);
    rect.setAttributeNS(null, 'y', startY);
    rect.setAttributeNS(null, 'width', coord.x - startX);
    rect.setAttributeNS(null, 'height', coord.y - startY);
    rect.setAttributeNS(null, 'fill', '#C4C4C4');
    currentSvg.appendChild(rect);
    currentRect = rect;
}

function renderRect(event) {
    console.log("render rect called");
    console.log("size is", coord.x - startX, coord.y - startY);
    if (coord.x - startX > 0) {
        currentRect.setAttributeNS(null, 'x', startX);
        currentRect.setAttributeNS(null, 'width', coord.x - startX + "px");
    } else {
        currentRect.setAttributeNS(null, 'x', coord.x);
        currentRect.setAttributeNS(null, 'width', startX - coord.x + "px");
    }

    if (coord.y - startY > 0) {
        currentRect.setAttributeNS(null, 'y', startY);
        currentRect.setAttributeNS(null, 'height', coord.y - startY + "px");
    } else {
        currentRect.setAttributeNS(null, 'y', coord.y);
        currentRect.setAttributeNS(null, 'height', startY - coord.y + "px");
    }
}

for (let i = 0; i < 2; i++) {
    console.log(getPageDiv(i + 1));
    setPageDivProperties(getPageDiv(i + 1), "1000px", "500px");
    setPageDivSvgEventListeners(getPageDiv(i + 1));
}