var isMousePressed = false;
var cEl;                    // current element being dragged
var startX, startY;         // x and y coord at the start of drag event
var cX, cY;                 // current X and current Y coordinate
function onMouseDown(event){
    console.log("draggable.js: onMouseDown called", event.target, event.currentTarget);
    isMousePressed = true;
    cEl = event.currentTarget;
    startX = event.clientX;
    startY = event.clientY;
    console.log("start top and left", cEl.style.top, cEl.style.left);
    window.addEventListener("mouseup", onMouseUp);
}

function onMouseUp(event){
    if(!isMousePressed){
        return;
    }
    console.log("draggable.js: onMouseUp called", event.target, event.currentTarget);
    isMousePressed = false;
    cEl = undefined;
    startX = undefined;
    startY = undefined;
    cX = undefined;
    cY = undefined;
    window.removeEventListener("mouseup", this);
}

function onMouseMove(event){
    if(!isMousePressed){
        return;
    }
    if(event.currentTarget != cEl){
        console.log("WARN: This was an expected error occured due to one element overlapping over other during drag");
    }
    console.log("draggable.js: onMouseMove called", event.target, event.currentTarget);
    cX = event.clientX;
    cY = event.clientY;
    console.log(cEl.style.left, cX, startX)
    cEl.style.left = parseInt(cEl.style.left.replace("px", "")) + (cX - startX) + "px";
    cEl.style.top = parseInt(cEl.style.top.replace("px", "")) + (cY - startY) + "px";
    startX = cX;
    startY = cY;
}

export function makeDraggable(el){
    el.addEventListener("mousedown", onMouseDown);
    el.addEventListener("mouseup", onMouseUp);
    el.addEventListener("mousemove", onMouseMove);
}

export function makeUndraggable(el){
    el.removeEventListener("mousedown", onMouseDown);
    el.removeEventListener("mouseup", onMouseUp);
    el.removeEventListener("mousemove", onMouseMove);
}