var isMousePressed = false;
let coord = { x: 0, y: 0 };    // this variable stores initial mouse co-ordinates
let currentCanvas;
let minX;
let maxX;
let minY;
let maxY;
let startX;
let startY;
// let ShapeType = {
//     UNK: 0,
//     FREE: 1,
//     RECT: 2,
//     ELLIPSE: 3,
//     CIRCLE: 4
// };
import { ShapeType } from './shapes';
console.log("Available shapetypes are", ShapeType);
let currentShape = ShapeType.UNK;
console.log("current shape is", currentShape);
var radios = document.querySelectorAll('input[type=radio][name="shape"]');

function changeHandler(event) {
    if (this.value === '1') {
        console.log('value', '1');
        currentShape = ShapeType.FREE;
    } else if (this.value === '2') {
        console.log('value', '2');
        currentShape = ShapeType.RECT;
    } else if (this.value === '3') {
        console.log('value', '3');
        currentShape = ShapeType.ELLIPSE;
    } else if (this.value === '4') {
        console.log('value', '4');
        currentShape = ShapeType.CIRCLE;
    }
    console.log("current shape is", currentShape);
}

Array.prototype.forEach.call(radios, function (radio) {
    radio.addEventListener('change', changeHandler);
});
function setCanvasProperties(canvas, height, width) {
    // IMPORTANT: don't set the height and width properties using CSS
    // REASON: the strokes appear at wrong position
    canvas.height = height;
    canvas.width = width;
}

function getCanvas(pageNumber) {
    return document.querySelector('div[data-page-number="' + pageNumber + '"] > canvas');
}

function setCanvasEventListeners(canvas) {
    // canvas.addEventListener("click", (event)=>{console.log(canvas.getBoundingClientRect()); console.log(event.clientX, event.clientY)});
    canvas.addEventListener("mousedown", startPainting);
    canvas.addEventListener("mouseup", stopPainting);
    canvas.addEventListener("mousemove", paint);
    canvas.addEventListener("mouseout", stopPainting);  // stop painting if mouse leaves the canvas
}

function updatePosition(event) {
    coord.x = event.clientX - currentCanvas.getBoundingClientRect().left;
    coord.y = event.clientY - currentCanvas.getBoundingClientRect().top;
}

function startPainting(event) {
    if (isMousePressed) {
        console.log("WARN: attempting to invoke a new paint session while their is another active paint session");
        return;
    }
    isMousePressed = true;
    currentCanvas = event.target;
    updatePosition(event);
    startX = coord.x;
    startY = coord.y;
    minX = coord.x;
    maxX = coord.x;
    minY = coord.y;
    maxY = coord.y;
}

function stopPainting(event) {
    if (!isMousePressed) {
        console.log("INFO: attempting to stop a paint session, while there are no acive paint session");
        return;
    }
    isMousePressed = false;
    let destCanvas = document.createElement('canvas');
    destCanvas.width = maxX - minX + 1;
    destCanvas.height = maxY - minY + 1;
    destCanvas.getContext("2d").drawImage(
        currentCanvas,
        minX, minY, maxX - minX, maxY - minY,  // source rect with content to crop
        0, 0, maxX - minX, maxY - minY);      // newCanvas, same size as source rect
    document.body.appendChild(destCanvas);
    currentCanvas = undefined;
    console.log("Bounding Box of object", minX, maxX, minY, maxY);
}

function paint(event) {
    if (!isMousePressed) return;
    if (currentCanvas != event.target) {
        console.log("something weird happened");
        return;
    }

    if (currentShape == ShapeType.FREE) {
        drawFree(event);
        // console.log(coord.x, coord.y);
        if (coord.x > maxX) {
            maxX = coord.x;
        }
        if (coord.x < minX) {
            minX = coord.x;
        }
        if (coord.y > maxY) {
            maxY = coord.y;
        }
        if (coord.y < minY) {
            minY = coord.y;
        }
    }

    if (currentShape == ShapeType.RECT) {
        drawRect(event);
        // console.log(coord.x, coord.y);
        minX = startX;
        minY = startY;
        maxX = coord.x;
        maxY = coord.y;
    }


}

function drawFree(event) {
    let ctx = currentCanvas.getContext("2d");
    ctx.beginPath();

    ctx.lineWidth = 5;

    // Sets the end of the lines drawn 
    // to a round shape. 
    ctx.lineCap = 'round';

    ctx.strokeStyle = 'green';

    // The cursor to start drawing 
    // moves to this coordinate 
    ctx.moveTo(coord.x, coord.y);
    // console.log(coord.x, coord.y); 
    // The position of the cursor 
    // gets updated as we move the 
    // mouse around. 
    updatePosition(event);
    // A line is traced from start 
    // coordinate to this coordinate 
    ctx.lineTo(coord.x, coord.y);

    // Draws the line. 
    ctx.stroke();
}

function drawRect(event) {
    let ctx = currentCanvas.getContext("2d");
    ctx.clearRect(0, 0, 500, 500);
    ctx.fillStyle = "blue";
    updatePosition(event);
    ctx.fillRect(startX, startY, coord.x - startX, coord.y - startY);
}

for (let i = 0; i < 2; i++) {
    console.log(getCanvas(i + 1));
    setCanvasProperties(getCanvas(i + 1), "500", "500");
    setCanvasEventListeners(getCanvas(i + 1));
}