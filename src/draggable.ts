var isMousePressed = false;
var cEl: HTMLElement;                    // current element being dragged
var startX: number, startY: number;         // x and y coord at the start of drag event
var cX: number, cY: number;                 // current X and current Y coordinate
function onMouseDown(event: MouseEvent){
    console.log("draggable.ts: onMouseDown called", event.target, event.currentTarget);
    isMousePressed = true;
    cEl = <HTMLElement>event.currentTarget;
    startX = event.clientX;
    startY = event.clientY;
    console.log("start top and left", cEl.style.top, cEl.style.left);
    window.addEventListener("mouseup", onMouseUp);
}

function onMouseUp(event: MouseEvent){
    if(!isMousePressed){
        return;
    }
    console.log("draggable.ts: onMouseUp called", event.target, event.currentTarget);
    window.removeEventListener("mouseup", this);
    let cEvent = new CustomEvent("cruxdragcomplete", {detail: {activity: (<any>cEl).getActivity()}});
    window.dispatchEvent(cEvent);
    isMousePressed = false;
    cEl = undefined;
    startX = undefined;
    startY = undefined;
    cX = undefined;
    cY = undefined;
}

function onMouseMove(event: MouseEvent){
    if(!isMousePressed){
        return;
    }
    if(event.currentTarget != cEl){
        console.log("WARN: This was an expected error occured due to one element overlapping over other during drag");
    }
    console.log("draggable.ts: onMouseMove called", event.target, event.currentTarget);
    cX = event.clientX;
    cY = event.clientY;
    console.log(cEl.style.left, cX, startX)
    cEl.style.left = parseInt(cEl.style.left.replace("px", "")) + (cX - startX) + "px";
    cEl.style.top = parseInt(cEl.style.top.replace("px", "")) + (cY - startY) + "px";
    startX = cX;
    startY = cY;
}

export function makeDraggable(el: HTMLElement){
    el.addEventListener("mousedown", onMouseDown);
    el.addEventListener("mouseup", onMouseUp);
    el.addEventListener("mousemove", onMouseMove);
}

export function makeUndraggable(el: HTMLElement){
    el.removeEventListener("mousedown", onMouseDown);
    el.removeEventListener("mouseup", onMouseUp);
    el.removeEventListener("mousemove", onMouseMove);
}