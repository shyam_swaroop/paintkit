import { SHAPE, Shape } from "./shape.interface";
import { Rect } from "./shapes/rect";
import { DrawEvent } from "./event";
import { makeDraggable, makeUndraggable } from './draggable';
/**
 * Activity needs a container and the shape intended to draw
 * Activity will add apt event listeners to the container
 * Activity will create PaintEvent object
 * It will pass the PaintEvent object to 
 */
enum State {
    READY,
    STARTED,
    FINISHED
};

export class Activity {
    container: HTMLElement;
    shapeType: SHAPE;
    shape: Shape;
    svg: SVGSVGElement;
    div: HTMLElement;
    config: ActivityConfig;
    activityState: State;
    private startX: number;
    private startY: number;
    private minX: number;
    private minY: number;
    private maxX: number;
    private maxY: number;
    private prevX: number;
    private prevY: number;
    private topHandle: HTMLElement;
    private rightHandle: HTMLElement;
    private leftHandle: HTMLElement;
    private bottomHandle: HTMLElement;
    constructor(container: HTMLElement, shapeType: SHAPE, config?: ActivityConfig) {
        this.container = container;
        this.shapeType = shapeType;
        if (this.shapeType == SHAPE.RECT) {
            console.log("[Activity] constructed rect shape")
            this.shape = new Rect();
            console.log(this.shape)
        }
        if (config) {
            this.config = config;
        } else {
            this.config = new ActivityConfig();
        }
        this.activityState = State.READY;
    }
    run() {
        this.createChildren();
        this.attachListenersToSvg();
    }
    createChildren() {
        this.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        this.svg.setAttribute('width', this.container.getBoundingClientRect().width + "");
        this.svg.setAttribute('height', this.container.getBoundingClientRect().height + "");
        this.svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
        this.div = document.createElement("div");
        this.div.style.width = this.container.getBoundingClientRect().width + "px";
        this.div.style.height = this.container.getBoundingClientRect().height + "px";
        console.log("position is", this.container.style.position)
        if(!this.container.style.position || this.container.style.position == "static"){
            this.container.style.position = "relative";
        }
        this.div.style.position = "absolute";
        this.container.appendChild(this.div);
        this.div.appendChild(this.svg);
        this.div.classList.add("cruxdrawelement");
        this.div.classList.add("cruxsvgcontainer");
        this.svg.classList.add("cruxdrawelement");
        this.svg.classList.add("cruxsvg");
        (<any>this.div).getActivity = ()=>{
            return this;
        }
        (<any>this.svg).getActivity = ()=>{
            return this;
        }
    }
    attachListenersToSvg() {
        this.svg.addEventListener("mousedown", this.startDrawing.bind(this));
        this.svg.addEventListener("mouseup", this.stopDrawing.bind(this));
        this.svg.addEventListener("mousemove", this.updateDrawing.bind(this));
    }
    attachListenersToWindow() {

    }
    startDrawing(event: MouseEvent) {
        if (this.activityState != State.READY) {
            console.warn("attempted to start an activity after it has been started or finished");
            return;
        }
        console.log("starting drawing");
        this.activityState = State.STARTED;
        this.startX = event.clientX - this.svg.getBoundingClientRect().left;
        this.startY = event.clientY - this.svg.getBoundingClientRect().top;
        this.updatePrev(event);
        this.updateMinMax(event);
        let drawEvent: DrawEvent = new DrawEvent(
            this.startX,
            this.startY,
            event.clientX - this.svg.getBoundingClientRect().left,
            event.clientY - this.svg.getBoundingClientRect().top,
            this.prevX,
            this.prevY
        );
        this.shape.create(this.svg, drawEvent);
        this.shape.applyStyle(this.config.shapeCSS);
    }
    stopDrawing(event: MouseEvent) {
        // Another approach can be to attach mousemove inside startDrawing
        if (this.activityState == State.READY) {
            return;
        }
        if (this.activityState == State.FINISHED) {
            console.warn("attempting to stop an already stopped activity");
            return;
        }
        console.log("stoping drawing");
        this.activityState = State.FINISHED;
        if (this.config.snapToShape) {
            console.log("snapping to shape", this.config);
            let gap = this.config.handleGap;
            let newX = parseInt(this.shape.left.replace("px", "")) - gap;
            let newY = parseInt(this.shape.top.replace("px", "")) - gap;
            let newWidth = parseInt(this.shape.width.replace("px", "")) + 2 * gap + "px";
            let newHeight = parseInt(this.shape.height.replace("px", "")) + 2 * gap + "px";
            this.div.style.top = newY + "px";
            this.div.style.left = newX + "px";
            this.div.style.height = newHeight;
            this.div.style.width = newWidth;
            this.div.style.display = "inline-block";
            this.svg.style.position = "relative";
            this.svg.style.left = gap+"px";
            this.svg.style.top = gap + "px";
            this.svg.setAttribute('width', this.shape.width);
            this.svg.setAttribute('height', this.shape.height);
            let newRectX = 0;
            let newRectY = 0;
            this.shape.left = newRectX + "";
            this.shape.top = newRectY + "";
            console.log("new rect attributes", newRectX, newRectY);
        }
        // remove all event listeners from svg
        let cloneSvg = <SVGSVGElement>this.svg.cloneNode(true);
        this.svg.parentNode.replaceChild(cloneSvg, this.svg);
        this.svg = cloneSvg;
        if(this.config.selectable){
            this.svg.addEventListener("mousedown", this.onSVGSelect.bind(this));
        }
        let cEvent = new CustomEvent("cruxactivitycomplete", {detail: {activity: this}});
        window.dispatchEvent(cEvent);
    }
    updateDrawing(event: MouseEvent) {
        if (this.activityState == State.READY) {
            return;
        }
        if (this.activityState == State.FINISHED) {
            console.warn("attempted to update drawing after the activity has finished");
            return;
        }
        console.log("update drawing", this.activityState, State.READY, State.STARTED, State.FINISHED)
        let drawEvent: DrawEvent = new DrawEvent(
            this.startX,
            this.startY,
            event.clientX - this.svg.getBoundingClientRect().left,
            event.clientY - this.svg.getBoundingClientRect().top,
            this.prevX,
            this.prevY
        );
        this.shape.draw(drawEvent);
        this.updatePrev(event);
    }
    updateMinMax(event: MouseEvent) {
        let newX = event.clientX, newY = event.clientY;
        if (this.minX > newX || !(this.minX)) {
            this.minX = newX;
        }
        if (this.maxX < newX || !(this.maxX)) {
            this.maxX = newX;
        }
        if (this.minY > newY || !(this.minY)) {
            this.minY = newY;
        }
        if (this.maxY < newY || !(this.maxY)) {
            this.maxY = newY;
        }
    }
    updatePrev(event: MouseEvent) {
        this.prevX = event.clientX - this.svg.getBoundingClientRect().left;
        this.prevY = event.clientY - this.svg.getBoundingClientRect().top;
    }
    onSVGSelect(event: MouseEvent){
        // check if the target is already selected
        let target = <HTMLElement>event.target;
        if(target.classList.contains("cruxselected")){
            return;
        }
        this.selectShape();
        function removeSelection(event: MouseEvent){
            let target = <HTMLElement>event.target;
            if(target != this.svg && target != this.div && target != this.svg.childNodes[0]){
                this.unselectShape();
                window.removeEventListener("mousedown", removeCB);
                console.log("removed event listeners on window");
                let cEvent = new CustomEvent("cruxsvgunselected", {detail: {activity: this}});
                window.dispatchEvent(cEvent);
            }
        }
        let removeCB = removeSelection.bind(this);
        window.addEventListener("mousedown", removeCB);
        let cEvent = new CustomEvent("cruxsvgselected", {detail: {activity: this}});
        window.dispatchEvent(cEvent);
    }
    selectShape(){
        // Add all handles and corners here only
        console.log("SVG selected");
        (<HTMLElement>this.svg.childNodes[0]).classList.add("cruxselected");
        this.svg.classList.add("cruxselected");
        this.div.classList.add("cruxselected");
        this.topHandle = createHandle("horizontal", parseInt(this.div.style.width.replace("px", "")) - 2*this.config.handleGap, this.config.handleGap, "", "");
        this.topHandle.style.position = "absolute";
        this.topHandle.style.top = "0";
        this.topHandle.style.left = this.config.handleGap + "px";
        this.div.appendChild(this.topHandle);
        this.bottomHandle = createHandle("horizontal", parseInt(this.div.style.width.replace("px", "")) - 2*this.config.handleGap, this.config.handleGap, "", "");
        this.bottomHandle.style.position = "absolute";
        this.bottomHandle.style.bottom = "0";
        this.bottomHandle.style.left = this.config.handleGap + "px";
        this.div.appendChild(this.bottomHandle);
        this.leftHandle = createHandle("vertical", this.config.handleGap, parseInt(this.div.style.height.replace("px", "")) - 2*this.config.handleGap, "", "");
        this.leftHandle.style.position = "absolute";
        this.leftHandle.style.top = this.config.handleGap + "px";
        this.leftHandle.style.left = "0";
        this.div.appendChild(this.leftHandle);
        this.rightHandle = createHandle("vertical", this.config.handleGap, parseInt(this.div.style.height.replace("px", "")) - 2*this.config.handleGap, "", "");
        this.rightHandle.style.position = "absolute";
        this.rightHandle.style.top = this.config.handleGap + "px";
        this.rightHandle.style.right = "0";
        this.div.appendChild(this.rightHandle);
        makeDraggable(this.div);
    }
    unselectShape(){
        // Remove all handles and corners here only
        console.log("SVG unselected");
        (<HTMLElement>this.svg.childNodes[0]).classList.remove("cruxselected");
        this.svg.classList.remove("cruxselected");
        this.div.classList.remove("cruxselected");
        this.leftHandle.parentElement.removeChild(this.leftHandle);
        this.leftHandle = undefined;
        this.rightHandle.parentElement.removeChild(this.rightHandle);
        this.rightHandle = undefined;
        this.topHandle.parentElement.removeChild(this.topHandle);
        this.topHandle = undefined;
        this.bottomHandle.parentElement.removeChild(this.bottomHandle);
        this.bottomHandle = undefined;
        makeUndraggable(this.div);
    }
    makeResizable(){

    }
    makeUnresizable(){

    }
    getPosition(): Position{
        let position: Position = new Position();
        position.top = parseInt(this.div.style.top.replace("px", ""));
        position.left = parseInt(this.div.style.left.replace("px", ""));
        position.height = parseInt(this.div.style.height.replace("px", ""));
        position.width = parseInt(this.div.style.width.replace("px", ""));
        return position;
    }
    getClientPosition(): Position{
        let position: Position = new Position();
        let divPos = this.getPosition();
        position.height = parseInt(this.svg.getAttributeNS(null, "height").replace("px", ""));
        position.width = parseInt(this.svg.getAttributeNS(null, "width").replace("px", ""));
        position.top = divPos.top + parseInt(this.svg.style.left.replace("px", ""));
        position.left = divPos.left + parseInt(this.svg.style.left.replace("px", ""));
        return position;
    }
}

export class ActivityConfig {
    handleGap: number;
    handleColor: string;
    handleStroke: string;
    cornerColor: string;
    shapeCSS: Object;
    selectable: boolean;
    resizable: boolean;
    draggable: boolean;
    snapToShape: boolean;
    constructor(
        shapeCss?: Object,
        handleGap?: number,
        handleColor?: string,
        handleStroke?: string,
        cornerColor?: string,
        selectable?: boolean,
        resizeable?: boolean,
        draggable?: boolean,
        snapToShape?: boolean
    ) {
        this.shapeCSS = shapeCss ? shapeCss : { fill: "rgba(196, 196, 196, 50)" };
        this.handleGap = handleGap ? handleGap : 10;
        this.handleColor = handleColor ? handleColor : "rgba(0, 0, 255, 50)";
        this.handleStroke = handleStroke ? handleStroke : "dashed";
        this.cornerColor = cornerColor ? cornerColor : "rgba(0, 0, 255, 100)";
        this.selectable = selectable ? selectable : true;
        this.resizable = resizeable ? resizeable : true;
        this.draggable = draggable ? draggable : true;
        this.snapToShape = snapToShape ? snapToShape : true;
    }
}

function createCorner(size: number, color: string): HTMLElement{
    return;
}

function createHandle(direction: string, length: number, height: number, color: string, stroke: string): HTMLElement{
    let handle = document.createElement("span");
    handle.classList.add("cruxdrawelement");
    handle.classList.add("cruxhandle");
    handle.style.width = length + "px";
    handle.style.height = height + "px";
    if(direction == "horizontal"){
        handle.style.background = "repeating-linear-gradient(to right, blue 0,blue 10px,transparent 10px,transparent 15px) center";
        handle.style.backgroundSize = "100% 3px";
        handle.style.backgroundRepeat = "no-repeat";
    }
    if(direction == "vertical"){
        handle.style.background = "repeating-linear-gradient(0deg,blue 0,blue 10px,transparent 1px,transparent 15px) repeat-y center top";
        handle.style.backgroundSize = "3px 100%";
    }
    return handle;
}

export class Position{
    top: number;
    left: number;
    height: number;
    width: number
}