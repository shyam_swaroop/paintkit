import { Shape } from "../shape.interface";
import { DrawEvent, DeltaEvent } from "../event";

export class Rect implements Shape{
    element: SVGSVGElement;
    constructor(){}
    applyStyle(style: any): void {
        let keys = Object.keys(style);
        for(let i=0; i < keys.length; i++){
            this.element.setAttributeNS(null, keys[i], style[keys[i]]);
        }
    }
    create(svg: SVGSVGElement, drawEvent: DrawEvent): SVGSVGElement {
        console.log("drawRect called");
        var svgns = "http://www.w3.org/2000/svg";
        this.element = <SVGSVGElement>document.createElementNS(svgns, 'rect');
        this.element.setAttributeNS(null, 'x', drawEvent.startX + "");
        this.element.setAttributeNS(null, 'y', drawEvent.startY + "");
        svg.appendChild(this.element);
        this.element.classList.add("cruxdrawelement");
        this.element.classList.add("cruxshape");
        return this.element;
    }
    draw(ev: DrawEvent): void {
        if (ev.currentX - ev.startX > 0) {
            this.element.setAttributeNS(null, 'x', ev.startX+"");
            this.element.setAttributeNS(null, 'width', ev.currentX - ev.startX + "px");
        } else {
            this.element.setAttributeNS(null, 'x', ev.currentX+"");
            this.element.setAttributeNS(null, 'width', ev.startX - ev.currentX + "px");
        }
    
        if (ev.currentY - ev.startY > 0) {
            this.element.setAttributeNS(null, 'y', ev.startY+"");
            this.element.setAttributeNS(null, 'height', ev.currentY - ev.startY + "px");
        } else {
            this.element.setAttributeNS(null, 'y', ev.currentY+"");
            this.element.setAttributeNS(null, 'height', ev.startY - ev.currentY + "px");
        }
    }
    moveDelta(delta: DeltaEvent): void {
        throw new Error("Method not implemented.");
    }
    resizeRight(delta: DeltaEvent): void {
        throw new Error("Method not implemented.");
    }
    resizeLeft(delta: DeltaEvent): void {
        throw new Error("Method not implemented.");
    }
    resizeUp(delta: DeltaEvent): void {
        throw new Error("Method not implemented.");
    }
    resizeDown(delta: DeltaEvent): void {
        throw new Error("Method not implemented.");
    }
    get top(): string{
        return this.element.getAttribute("y");
    }
    get left(): string{
        return this.element.getAttribute("x");
    }
    get width(): string{
        return this.element.getAttribute("width");
    }
    get height(): string{
        return this.element.getAttribute("height");
    }
    set top(value: string){
        this.element.setAttributeNS(null, "y", value);
    }
    set left(value: string){
        this.element.setAttributeNS(null, "x", value);
    }
    set height(value: string){
        this.element.setAttributeNS(null, "height", value);
    }
    set width(value: string){
        this.element.setAttributeNS(null, "width", value);
    }
    setAttribute(attr: string, value: string){
        this.element.setAttributeNS(null, attr, value);
    }
}