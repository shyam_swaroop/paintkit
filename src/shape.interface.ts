import { DrawEvent, DeltaEvent } from "./event";

export enum SHAPE {
    RECT,
    ELLIPSE
}

export interface Shape {
    create(svg: SVGSVGElement, ev: DrawEvent): SVGSVGElement;
    draw(ev: DrawEvent): void;
    moveDelta(delta: DeltaEvent): void;
    resizeRight(delta: DeltaEvent): void;
    resizeLeft(delta: DeltaEvent): void;
    resizeUp(delta: DeltaEvent): void;
    resizeDown(delta: DeltaEvent): void;
    applyStyle(style: Object): void;
    top: string;
    left: string;
    width: string;
    height: string;
    setAttribute(attr: string, value: string): void;
}