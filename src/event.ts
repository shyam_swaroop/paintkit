/**
 * PaintEvent will be required all shapes to draw themselves
 */
export class DrawEvent {
    startX: number;
    startY: number;
    currentX: number;
    currentY: number;
    prevX: number;
    prevY: number;
    constructor(
        startX: number,
        startY: number,
        currentX: number,
        currentY: number,
        prevX: number,
        prevY: number
    ) {
        this.startX = startX;
        this.startY = startY;
        this.currentX = currentX;
        this.currentY = currentY;
        this.prevX = prevX;
        this.prevY = prevY;
    }
}

export class DeltaEvent {
    x: number;
    y: number;
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}