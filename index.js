const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');

app.get('/', (req, res)=>{
    let file = req.query["file"];
    console.log(file);
    if(!fs.existsSync(path.resolve(file))){
        res.send("File doesn't exist");
        return;
    }
    res.sendFile(path.resolve(file));
});

app.listen(4444, ()=>{
    console.log("server started at port", 4444);
});