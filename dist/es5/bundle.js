(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Position = exports.ActivityConfig = exports.Activity = void 0;
var shape_interface_1 = require("./shape.interface");
var rect_1 = require("./shapes/rect");
var event_1 = require("./event");
var draggable_1 = require("./draggable");
/**
 * Activity needs a container and the shape intended to draw
 * Activity will add apt event listeners to the container
 * Activity will create PaintEvent object
 * It will pass the PaintEvent object to
 */
var State;
(function (State) {
    State[State["READY"] = 0] = "READY";
    State[State["STARTED"] = 1] = "STARTED";
    State[State["FINISHED"] = 2] = "FINISHED";
})(State || (State = {}));
;
var Activity = /** @class */ (function () {
    function Activity(container, shapeType, config) {
        this.container = container;
        this.shapeType = shapeType;
        if (this.shapeType == shape_interface_1.SHAPE.RECT) {
            console.log("[Activity] constructed rect shape");
            this.shape = new rect_1.Rect();
            console.log(this.shape);
        }
        if (config) {
            this.config = config;
        }
        else {
            this.config = new ActivityConfig();
        }
        this.activityState = State.READY;
    }
    Activity.prototype.run = function () {
        this.createChildren();
        this.attachListenersToSvg();
    };
    Activity.prototype.createChildren = function () {
        var _this = this;
        this.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        this.svg.setAttribute('width', this.container.getBoundingClientRect().width + "");
        this.svg.setAttribute('height', this.container.getBoundingClientRect().height + "");
        this.svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
        this.div = document.createElement("div");
        this.div.style.width = this.container.getBoundingClientRect().width + "px";
        this.div.style.height = this.container.getBoundingClientRect().height + "px";
        console.log("position is", this.container.style.position);
        if (!this.container.style.position || this.container.style.position == "static") {
            this.container.style.position = "relative";
        }
        this.div.style.position = "absolute";
        this.container.appendChild(this.div);
        this.div.appendChild(this.svg);
        this.div.classList.add("cruxdrawelement");
        this.div.classList.add("cruxsvgcontainer");
        this.svg.classList.add("cruxdrawelement");
        this.svg.classList.add("cruxsvg");
        this.div.getActivity = function () {
            return _this;
        };
        this.svg.getActivity = function () {
            return _this;
        };
    };
    Activity.prototype.attachListenersToSvg = function () {
        this.svg.addEventListener("mousedown", this.startDrawing.bind(this));
        this.svg.addEventListener("mouseup", this.stopDrawing.bind(this));
        this.svg.addEventListener("mousemove", this.updateDrawing.bind(this));
    };
    Activity.prototype.attachListenersToWindow = function () {
    };
    Activity.prototype.startDrawing = function (event) {
        if (this.activityState != State.READY) {
            console.warn("attempted to start an activity after it has been started or finished");
            return;
        }
        console.log("starting drawing");
        this.activityState = State.STARTED;
        this.startX = event.clientX - this.svg.getBoundingClientRect().left;
        this.startY = event.clientY - this.svg.getBoundingClientRect().top;
        this.updatePrev(event);
        this.updateMinMax(event);
        var drawEvent = new event_1.DrawEvent(this.startX, this.startY, event.clientX - this.svg.getBoundingClientRect().left, event.clientY - this.svg.getBoundingClientRect().top, this.prevX, this.prevY);
        this.shape.create(this.svg, drawEvent);
        this.shape.applyStyle(this.config.shapeCSS);
    };
    Activity.prototype.stopDrawing = function (event) {
        // Another approach can be to attach mousemove inside startDrawing
        if (this.activityState == State.READY) {
            return;
        }
        if (this.activityState == State.FINISHED) {
            console.warn("attempting to stop an already stopped activity");
            return;
        }
        console.log("stoping drawing");
        this.activityState = State.FINISHED;
        if (this.config.snapToShape) {
            console.log("snapping to shape", this.config);
            var gap = this.config.handleGap;
            var newX = parseInt(this.shape.left.replace("px", "")) - gap;
            var newY = parseInt(this.shape.top.replace("px", "")) - gap;
            var newWidth = parseInt(this.shape.width.replace("px", "")) + 2 * gap + "px";
            var newHeight = parseInt(this.shape.height.replace("px", "")) + 2 * gap + "px";
            this.div.style.top = newY + "px";
            this.div.style.left = newX + "px";
            this.div.style.height = newHeight;
            this.div.style.width = newWidth;
            this.div.style.display = "inline-block";
            this.svg.style.position = "relative";
            this.svg.style.left = gap + "px";
            this.svg.style.top = gap + "px";
            this.svg.setAttribute('width', this.shape.width);
            this.svg.setAttribute('height', this.shape.height);
            var newRectX = 0;
            var newRectY = 0;
            this.shape.left = newRectX + "";
            this.shape.top = newRectY + "";
            console.log("new rect attributes", newRectX, newRectY);
        }
        // remove all event listeners from svg
        var cloneSvg = this.svg.cloneNode(true);
        this.svg.parentNode.replaceChild(cloneSvg, this.svg);
        this.svg = cloneSvg;
        if (this.config.selectable) {
            this.svg.addEventListener("mousedown", this.onSVGSelect.bind(this));
        }
        var cEvent = new CustomEvent("cruxactivitycomplete", { detail: { activity: this } });
        window.dispatchEvent(cEvent);
    };
    Activity.prototype.updateDrawing = function (event) {
        if (this.activityState == State.READY) {
            return;
        }
        if (this.activityState == State.FINISHED) {
            console.warn("attempted to update drawing after the activity has finished");
            return;
        }
        console.log("update drawing", this.activityState, State.READY, State.STARTED, State.FINISHED);
        var drawEvent = new event_1.DrawEvent(this.startX, this.startY, event.clientX - this.svg.getBoundingClientRect().left, event.clientY - this.svg.getBoundingClientRect().top, this.prevX, this.prevY);
        this.shape.draw(drawEvent);
        this.updatePrev(event);
    };
    Activity.prototype.updateMinMax = function (event) {
        var newX = event.clientX, newY = event.clientY;
        if (this.minX > newX || !(this.minX)) {
            this.minX = newX;
        }
        if (this.maxX < newX || !(this.maxX)) {
            this.maxX = newX;
        }
        if (this.minY > newY || !(this.minY)) {
            this.minY = newY;
        }
        if (this.maxY < newY || !(this.maxY)) {
            this.maxY = newY;
        }
    };
    Activity.prototype.updatePrev = function (event) {
        this.prevX = event.clientX - this.svg.getBoundingClientRect().left;
        this.prevY = event.clientY - this.svg.getBoundingClientRect().top;
    };
    Activity.prototype.onSVGSelect = function (event) {
        // check if the target is already selected
        var target = event.target;
        if (target.classList.contains("cruxselected")) {
            return;
        }
        this.selectShape();
        function removeSelection(event) {
            var target = event.target;
            if (target != this.svg && target != this.div && target != this.svg.childNodes[0]) {
                this.unselectShape();
                window.removeEventListener("mousedown", removeCB);
                console.log("removed event listeners on window");
                var cEvent_1 = new CustomEvent("cruxsvgunselected", { detail: { activity: this } });
                window.dispatchEvent(cEvent_1);
            }
        }
        var removeCB = removeSelection.bind(this);
        window.addEventListener("mousedown", removeCB);
        var cEvent = new CustomEvent("cruxsvgselected", { detail: { activity: this } });
        window.dispatchEvent(cEvent);
    };
    Activity.prototype.selectShape = function () {
        // Add all handles and corners here only
        console.log("SVG selected");
        this.svg.childNodes[0].classList.add("cruxselected");
        this.svg.classList.add("cruxselected");
        this.div.classList.add("cruxselected");
        this.topHandle = createHandle("horizontal", parseInt(this.div.style.width.replace("px", "")) - 2 * this.config.handleGap, this.config.handleGap, "", "");
        this.topHandle.style.position = "absolute";
        this.topHandle.style.top = "0";
        this.topHandle.style.left = this.config.handleGap + "px";
        this.div.appendChild(this.topHandle);
        this.bottomHandle = createHandle("horizontal", parseInt(this.div.style.width.replace("px", "")) - 2 * this.config.handleGap, this.config.handleGap, "", "");
        this.bottomHandle.style.position = "absolute";
        this.bottomHandle.style.bottom = "0";
        this.bottomHandle.style.left = this.config.handleGap + "px";
        this.div.appendChild(this.bottomHandle);
        this.leftHandle = createHandle("vertical", this.config.handleGap, parseInt(this.div.style.height.replace("px", "")) - 2 * this.config.handleGap, "", "");
        this.leftHandle.style.position = "absolute";
        this.leftHandle.style.top = this.config.handleGap + "px";
        this.leftHandle.style.left = "0";
        this.div.appendChild(this.leftHandle);
        this.rightHandle = createHandle("vertical", this.config.handleGap, parseInt(this.div.style.height.replace("px", "")) - 2 * this.config.handleGap, "", "");
        this.rightHandle.style.position = "absolute";
        this.rightHandle.style.top = this.config.handleGap + "px";
        this.rightHandle.style.right = "0";
        this.div.appendChild(this.rightHandle);
        draggable_1.makeDraggable(this.div);
    };
    Activity.prototype.unselectShape = function () {
        // Remove all handles and corners here only
        console.log("SVG unselected");
        this.svg.childNodes[0].classList.remove("cruxselected");
        this.svg.classList.remove("cruxselected");
        this.div.classList.remove("cruxselected");
        this.leftHandle.parentElement.removeChild(this.leftHandle);
        this.leftHandle = undefined;
        this.rightHandle.parentElement.removeChild(this.rightHandle);
        this.rightHandle = undefined;
        this.topHandle.parentElement.removeChild(this.topHandle);
        this.topHandle = undefined;
        this.bottomHandle.parentElement.removeChild(this.bottomHandle);
        this.bottomHandle = undefined;
        draggable_1.makeUndraggable(this.div);
    };
    Activity.prototype.makeResizable = function () {
    };
    Activity.prototype.makeUnresizable = function () {
    };
    Activity.prototype.getPosition = function () {
        var position = new Position();
        position.top = parseInt(this.div.style.top.replace("px", ""));
        position.left = parseInt(this.div.style.left.replace("px", ""));
        position.height = parseInt(this.div.style.height.replace("px", ""));
        position.width = parseInt(this.div.style.width.replace("px", ""));
        return position;
    };
    Activity.prototype.getClientPosition = function () {
        var position = new Position();
        var divPos = this.getPosition();
        position.height = parseInt(this.svg.getAttributeNS(null, "height").replace("px", ""));
        position.width = parseInt(this.svg.getAttributeNS(null, "width").replace("px", ""));
        position.top = divPos.top + parseInt(this.svg.style.left.replace("px", ""));
        position.left = divPos.left + parseInt(this.svg.style.left.replace("px", ""));
        return position;
    };
    return Activity;
}());
exports.Activity = Activity;
var ActivityConfig = /** @class */ (function () {
    function ActivityConfig(shapeCss, handleGap, handleColor, handleStroke, cornerColor, selectable, resizeable, draggable, snapToShape) {
        this.shapeCSS = shapeCss ? shapeCss : { fill: "rgba(196, 196, 196, 50)" };
        this.handleGap = handleGap ? handleGap : 10;
        this.handleColor = handleColor ? handleColor : "rgba(0, 0, 255, 50)";
        this.handleStroke = handleStroke ? handleStroke : "dashed";
        this.cornerColor = cornerColor ? cornerColor : "rgba(0, 0, 255, 100)";
        this.selectable = selectable ? selectable : true;
        this.resizable = resizeable ? resizeable : true;
        this.draggable = draggable ? draggable : true;
        this.snapToShape = snapToShape ? snapToShape : true;
    }
    return ActivityConfig;
}());
exports.ActivityConfig = ActivityConfig;
function createCorner(size, color) {
    return;
}
function createHandle(direction, length, height, color, stroke) {
    var handle = document.createElement("span");
    handle.classList.add("cruxdrawelement");
    handle.classList.add("cruxhandle");
    handle.style.width = length + "px";
    handle.style.height = height + "px";
    if (direction == "horizontal") {
        handle.style.background = "repeating-linear-gradient(to right, blue 0,blue 10px,transparent 10px,transparent 15px) center";
        handle.style.backgroundSize = "100% 3px";
        handle.style.backgroundRepeat = "no-repeat";
    }
    if (direction == "vertical") {
        handle.style.background = "repeating-linear-gradient(0deg,blue 0,blue 10px,transparent 1px,transparent 15px) repeat-y center top";
        handle.style.backgroundSize = "3px 100%";
    }
    return handle;
}
var Position = /** @class */ (function () {
    function Position() {
    }
    return Position;
}());
exports.Position = Position;

},{"./draggable":2,"./event":3,"./shape.interface":5,"./shapes/rect":6}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeUndraggable = exports.makeDraggable = void 0;
var isMousePressed = false;
var cEl; // current element being dragged
var startX, startY; // x and y coord at the start of drag event
var cX, cY; // current X and current Y coordinate
function onMouseDown(event) {
    console.log("draggable.ts: onMouseDown called", event.target, event.currentTarget);
    isMousePressed = true;
    cEl = event.currentTarget;
    startX = event.clientX;
    startY = event.clientY;
    console.log("start top and left", cEl.style.top, cEl.style.left);
    window.addEventListener("mouseup", onMouseUp);
}
function onMouseUp(event) {
    if (!isMousePressed) {
        return;
    }
    console.log("draggable.ts: onMouseUp called", event.target, event.currentTarget);
    window.removeEventListener("mouseup", this);
    var cEvent = new CustomEvent("cruxdragcomplete", { detail: { activity: cEl.getActivity() } });
    window.dispatchEvent(cEvent);
    isMousePressed = false;
    cEl = undefined;
    startX = undefined;
    startY = undefined;
    cX = undefined;
    cY = undefined;
}
function onMouseMove(event) {
    if (!isMousePressed) {
        return;
    }
    if (event.currentTarget != cEl) {
        console.log("WARN: This was an expected error occured due to one element overlapping over other during drag");
    }
    console.log("draggable.ts: onMouseMove called", event.target, event.currentTarget);
    cX = event.clientX;
    cY = event.clientY;
    console.log(cEl.style.left, cX, startX);
    cEl.style.left = parseInt(cEl.style.left.replace("px", "")) + (cX - startX) + "px";
    cEl.style.top = parseInt(cEl.style.top.replace("px", "")) + (cY - startY) + "px";
    startX = cX;
    startY = cY;
}
function makeDraggable(el) {
    el.addEventListener("mousedown", onMouseDown);
    el.addEventListener("mouseup", onMouseUp);
    el.addEventListener("mousemove", onMouseMove);
}
exports.makeDraggable = makeDraggable;
function makeUndraggable(el) {
    el.removeEventListener("mousedown", onMouseDown);
    el.removeEventListener("mouseup", onMouseUp);
    el.removeEventListener("mousemove", onMouseMove);
}
exports.makeUndraggable = makeUndraggable;

},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeltaEvent = exports.DrawEvent = void 0;
/**
 * PaintEvent will be required all shapes to draw themselves
 */
var DrawEvent = /** @class */ (function () {
    function DrawEvent(startX, startY, currentX, currentY, prevX, prevY) {
        this.startX = startX;
        this.startY = startY;
        this.currentX = currentX;
        this.currentY = currentY;
        this.prevX = prevX;
        this.prevY = prevY;
    }
    return DrawEvent;
}());
exports.DrawEvent = DrawEvent;
var DeltaEvent = /** @class */ (function () {
    function DeltaEvent(x, y) {
        this.x = x;
        this.y = y;
    }
    return DeltaEvent;
}());
exports.DeltaEvent = DeltaEvent;

},{}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var activity_1 = require("./activity");
if (window) {
    window.Art = {
        Activity: activity_1.Activity,
        ActivityConfig: activity_1.ActivityConfig
    };
}

},{"./activity":1}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SHAPE = void 0;
var SHAPE;
(function (SHAPE) {
    SHAPE[SHAPE["RECT"] = 0] = "RECT";
    SHAPE[SHAPE["ELLIPSE"] = 1] = "ELLIPSE";
})(SHAPE = exports.SHAPE || (exports.SHAPE = {}));

},{}],6:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rect = void 0;
var Rect = /** @class */ (function () {
    function Rect() {
    }
    Rect.prototype.applyStyle = function (style) {
        var keys = Object.keys(style);
        for (var i = 0; i < keys.length; i++) {
            this.element.setAttributeNS(null, keys[i], style[keys[i]]);
        }
    };
    Rect.prototype.create = function (svg, drawEvent) {
        console.log("drawRect called");
        var svgns = "http://www.w3.org/2000/svg";
        this.element = document.createElementNS(svgns, 'rect');
        this.element.setAttributeNS(null, 'x', drawEvent.startX + "");
        this.element.setAttributeNS(null, 'y', drawEvent.startY + "");
        svg.appendChild(this.element);
        this.element.classList.add("cruxdrawelement");
        this.element.classList.add("cruxshape");
        return this.element;
    };
    Rect.prototype.draw = function (ev) {
        if (ev.currentX - ev.startX > 0) {
            this.element.setAttributeNS(null, 'x', ev.startX + "");
            this.element.setAttributeNS(null, 'width', ev.currentX - ev.startX + "px");
        }
        else {
            this.element.setAttributeNS(null, 'x', ev.currentX + "");
            this.element.setAttributeNS(null, 'width', ev.startX - ev.currentX + "px");
        }
        if (ev.currentY - ev.startY > 0) {
            this.element.setAttributeNS(null, 'y', ev.startY + "");
            this.element.setAttributeNS(null, 'height', ev.currentY - ev.startY + "px");
        }
        else {
            this.element.setAttributeNS(null, 'y', ev.currentY + "");
            this.element.setAttributeNS(null, 'height', ev.startY - ev.currentY + "px");
        }
    };
    Rect.prototype.moveDelta = function (delta) {
        throw new Error("Method not implemented.");
    };
    Rect.prototype.resizeRight = function (delta) {
        throw new Error("Method not implemented.");
    };
    Rect.prototype.resizeLeft = function (delta) {
        throw new Error("Method not implemented.");
    };
    Rect.prototype.resizeUp = function (delta) {
        throw new Error("Method not implemented.");
    };
    Rect.prototype.resizeDown = function (delta) {
        throw new Error("Method not implemented.");
    };
    Object.defineProperty(Rect.prototype, "top", {
        get: function () {
            return this.element.getAttribute("y");
        },
        set: function (value) {
            this.element.setAttributeNS(null, "y", value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rect.prototype, "left", {
        get: function () {
            return this.element.getAttribute("x");
        },
        set: function (value) {
            this.element.setAttributeNS(null, "x", value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rect.prototype, "width", {
        get: function () {
            return this.element.getAttribute("width");
        },
        set: function (value) {
            this.element.setAttributeNS(null, "width", value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rect.prototype, "height", {
        get: function () {
            return this.element.getAttribute("height");
        },
        set: function (value) {
            this.element.setAttributeNS(null, "height", value);
        },
        enumerable: false,
        configurable: true
    });
    Rect.prototype.setAttribute = function (attr, value) {
        this.element.setAttributeNS(null, attr, value);
    };
    return Rect;
}());
exports.Rect = Rect;

},{}]},{},[4])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvYWN0aXZpdHkudHMiLCJzcmMvZHJhZ2dhYmxlLnRzIiwic3JjL2V2ZW50LnRzIiwic3JjL3B1YmxpYy50cyIsInNyYy9zaGFwZS5pbnRlcmZhY2UudHMiLCJzcmMvc2hhcGVzL3JlY3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7QUNBQSxxREFBaUQ7QUFDakQsc0NBQXFDO0FBQ3JDLGlDQUFvQztBQUNwQyx5Q0FBNkQ7QUFDN0Q7Ozs7O0dBS0c7QUFDSCxJQUFLLEtBSUo7QUFKRCxXQUFLLEtBQUs7SUFDTixtQ0FBSyxDQUFBO0lBQ0wsdUNBQU8sQ0FBQTtJQUNQLHlDQUFRLENBQUE7QUFDWixDQUFDLEVBSkksS0FBSyxLQUFMLEtBQUssUUFJVDtBQUFBLENBQUM7QUFFRjtJQW9CSSxrQkFBWSxTQUFzQixFQUFFLFNBQWdCLEVBQUUsTUFBdUI7UUFDekUsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDM0IsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLHVCQUFLLENBQUMsSUFBSSxFQUFFO1lBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQTtZQUNoRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksV0FBSSxFQUFFLENBQUM7WUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDMUI7UUFDRCxJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1NBQ3hCO2FBQU07WUFDSCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7U0FDdEM7UUFDRCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7SUFDckMsQ0FBQztJQUNELHNCQUFHLEdBQUg7UUFDSSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUNELGlDQUFjLEdBQWQ7UUFBQSxpQkF5QkM7UUF4QkcsSUFBSSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDLDRCQUE0QixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixFQUFFLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixFQUFFLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ3BGLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLCtCQUErQixFQUFFLGFBQWEsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1FBQ3hHLElBQUksQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDM0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzdFLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQ3pELElBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLFFBQVEsRUFBQztZQUMzRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO1NBQzlDO1FBQ0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztRQUNyQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsR0FBSSxDQUFDLFdBQVcsR0FBRztZQUMxQixPQUFPLEtBQUksQ0FBQztRQUNoQixDQUFDLENBQUE7UUFDSyxJQUFJLENBQUMsR0FBSSxDQUFDLFdBQVcsR0FBRztZQUMxQixPQUFPLEtBQUksQ0FBQztRQUNoQixDQUFDLENBQUE7SUFDTCxDQUFDO0lBQ0QsdUNBQW9CLEdBQXBCO1FBQ0ksSUFBSSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUNELDBDQUF1QixHQUF2QjtJQUVBLENBQUM7SUFDRCwrQkFBWSxHQUFaLFVBQWEsS0FBaUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDbkMsT0FBTyxDQUFDLElBQUksQ0FBQyxzRUFBc0UsQ0FBQyxDQUFDO1lBQ3JGLE9BQU87U0FDVjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxJQUFJLENBQUM7UUFDcEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7UUFDbkUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pCLElBQUksU0FBUyxHQUFjLElBQUksaUJBQVMsQ0FDcEMsSUFBSSxDQUFDLE1BQU0sRUFDWCxJQUFJLENBQUMsTUFBTSxFQUNYLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLElBQUksRUFDckQsS0FBSyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxFQUNwRCxJQUFJLENBQUMsS0FBSyxFQUNWLElBQUksQ0FBQyxLQUFLLENBQ2IsQ0FBQztRQUNGLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBQ0QsOEJBQVcsR0FBWCxVQUFZLEtBQWlCO1FBQ3pCLGtFQUFrRTtRQUNsRSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNuQyxPQUFPO1NBQ1Y7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksS0FBSyxDQUFDLFFBQVEsRUFBRTtZQUN0QyxPQUFPLENBQUMsSUFBSSxDQUFDLGdEQUFnRCxDQUFDLENBQUM7WUFDL0QsT0FBTztTQUNWO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQztRQUNwQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ2hDLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQzdELElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQzVELElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUM7WUFDN0UsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQztZQUMvRSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNqQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNsQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUM7WUFDaEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQztZQUN4QyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUMsSUFBSSxDQUFDO1lBQy9CLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25ELElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQztZQUNqQixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQzFEO1FBQ0Qsc0NBQXNDO1FBQ3RDLElBQUksUUFBUSxHQUFrQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztRQUNwQixJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFDO1lBQ3RCLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDdkU7UUFDRCxJQUFJLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxzQkFBc0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsRUFBQyxDQUFDLENBQUM7UUFDakYsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBQ0QsZ0NBQWEsR0FBYixVQUFjLEtBQWlCO1FBQzNCLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ25DLE9BQU87U0FDVjtRQUNELElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxLQUFLLENBQUMsUUFBUSxFQUFFO1lBQ3RDLE9BQU8sQ0FBQyxJQUFJLENBQUMsNkRBQTZELENBQUMsQ0FBQztZQUM1RSxPQUFPO1NBQ1Y7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUM3RixJQUFJLFNBQVMsR0FBYyxJQUFJLGlCQUFTLENBQ3BDLElBQUksQ0FBQyxNQUFNLEVBQ1gsSUFBSSxDQUFDLE1BQU0sRUFDWCxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxJQUFJLEVBQ3JELEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsRUFDcEQsSUFBSSxDQUFDLEtBQUssRUFDVixJQUFJLENBQUMsS0FBSyxDQUNiLENBQUM7UUFDRixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFDRCwrQkFBWSxHQUFaLFVBQWEsS0FBaUI7UUFDMUIsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLE9BQU8sRUFBRSxJQUFJLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUMvQyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7U0FDcEI7SUFDTCxDQUFDO0lBQ0QsNkJBQVUsR0FBVixVQUFXLEtBQWlCO1FBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSSxDQUFDO1FBQ25FLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO0lBQ3RFLENBQUM7SUFDRCw4QkFBVyxHQUFYLFVBQVksS0FBaUI7UUFDekIsMENBQTBDO1FBQzFDLElBQUksTUFBTSxHQUFnQixLQUFLLENBQUMsTUFBTSxDQUFDO1FBQ3ZDLElBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUM7WUFDekMsT0FBTztTQUNWO1FBQ0QsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLFNBQVMsZUFBZSxDQUFDLEtBQWlCO1lBQ3RDLElBQUksTUFBTSxHQUFnQixLQUFLLENBQUMsTUFBTSxDQUFDO1lBQ3ZDLElBQUcsTUFBTSxJQUFJLElBQUksQ0FBQyxHQUFHLElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxHQUFHLElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFDO2dCQUM1RSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ3JCLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQztnQkFDakQsSUFBSSxRQUFNLEdBQUcsSUFBSSxXQUFXLENBQUMsbUJBQW1CLEVBQUUsRUFBQyxNQUFNLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLEVBQUMsQ0FBQyxDQUFDO2dCQUM5RSxNQUFNLENBQUMsYUFBYSxDQUFDLFFBQU0sQ0FBQyxDQUFDO2FBQ2hDO1FBQ0wsQ0FBQztRQUNELElBQUksUUFBUSxHQUFHLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMvQyxJQUFJLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLE1BQU0sRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsRUFBQyxDQUFDLENBQUM7UUFDNUUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBQ0QsOEJBQVcsR0FBWDtRQUNJLHdDQUF3QztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFFLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZKLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7UUFDM0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3pELElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMxSixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO1FBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDckMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUM1RCxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxZQUFZLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDdkosSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztRQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3pELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3hKLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7UUFDN0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUMxRCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQ25DLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN2Qyx5QkFBYSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBQ0QsZ0NBQWEsR0FBYjtRQUNJLDJDQUEyQztRQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDNUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQzNCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7UUFDOUIsMkJBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUNELGdDQUFhLEdBQWI7SUFFQSxDQUFDO0lBQ0Qsa0NBQWUsR0FBZjtJQUVBLENBQUM7SUFDRCw4QkFBVyxHQUFYO1FBQ0ksSUFBSSxRQUFRLEdBQWEsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUN4QyxRQUFRLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlELFFBQVEsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDaEUsUUFBUSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRSxRQUFRLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFDRCxvQ0FBaUIsR0FBakI7UUFDSSxJQUFJLFFBQVEsR0FBYSxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQ3hDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNoQyxRQUFRLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3RGLFFBQVEsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEYsUUFBUSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzVFLFFBQVEsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5RSxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBQ0wsZUFBQztBQUFELENBM1FBLEFBMlFDLElBQUE7QUEzUVksNEJBQVE7QUE2UXJCO0lBVUksd0JBQ0ksUUFBaUIsRUFDakIsU0FBa0IsRUFDbEIsV0FBb0IsRUFDcEIsWUFBcUIsRUFDckIsV0FBb0IsRUFDcEIsVUFBb0IsRUFDcEIsVUFBb0IsRUFDcEIsU0FBbUIsRUFDbkIsV0FBcUI7UUFFckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUseUJBQXlCLEVBQUUsQ0FBQztRQUMxRSxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDNUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUM7UUFDckUsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQzNELElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDO1FBQ3RFLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNqRCxJQUFJLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzlDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN4RCxDQUFDO0lBQ0wscUJBQUM7QUFBRCxDQS9CQSxBQStCQyxJQUFBO0FBL0JZLHdDQUFjO0FBaUMzQixTQUFTLFlBQVksQ0FBQyxJQUFZLEVBQUUsS0FBYTtJQUM3QyxPQUFPO0FBQ1gsQ0FBQztBQUVELFNBQVMsWUFBWSxDQUFDLFNBQWlCLEVBQUUsTUFBYyxFQUFFLE1BQWMsRUFBRSxLQUFhLEVBQUUsTUFBYztJQUNsRyxJQUFJLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzVDLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDeEMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDbkMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNuQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ3BDLElBQUcsU0FBUyxJQUFJLFlBQVksRUFBQztRQUN6QixNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxnR0FBZ0csQ0FBQztRQUMzSCxNQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUM7UUFDekMsTUFBTSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxXQUFXLENBQUM7S0FDL0M7SUFDRCxJQUFHLFNBQVMsSUFBSSxVQUFVLEVBQUM7UUFDdkIsTUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsdUdBQXVHLENBQUM7UUFDbEksTUFBTSxDQUFDLEtBQUssQ0FBQyxjQUFjLEdBQUcsVUFBVSxDQUFDO0tBQzVDO0lBQ0QsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQztBQUVEO0lBQUE7SUFLQSxDQUFDO0lBQUQsZUFBQztBQUFELENBTEEsQUFLQyxJQUFBO0FBTFksNEJBQVE7Ozs7OztBQ3BWckIsSUFBSSxjQUFjLEdBQUcsS0FBSyxDQUFDO0FBQzNCLElBQUksR0FBZ0IsQ0FBQyxDQUFvQixnQ0FBZ0M7QUFDekUsSUFBSSxNQUFjLEVBQUUsTUFBYyxDQUFDLENBQVMsMkNBQTJDO0FBQ3ZGLElBQUksRUFBVSxFQUFFLEVBQVUsQ0FBQyxDQUFpQixxQ0FBcUM7QUFDakYsU0FBUyxXQUFXLENBQUMsS0FBaUI7SUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsRUFBRSxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNuRixjQUFjLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLEdBQUcsR0FBZ0IsS0FBSyxDQUFDLGFBQWEsQ0FBQztJQUN2QyxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztJQUN2QixNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztJQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakUsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztBQUNsRCxDQUFDO0FBRUQsU0FBUyxTQUFTLENBQUMsS0FBaUI7SUFDaEMsSUFBRyxDQUFDLGNBQWMsRUFBQztRQUNmLE9BQU87S0FDVjtJQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLEVBQUUsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDakYsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM1QyxJQUFJLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxrQkFBa0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxFQUFDLFFBQVEsRUFBUSxHQUFJLENBQUMsV0FBVyxFQUFFLEVBQUMsRUFBQyxDQUFDLENBQUM7SUFDakcsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM3QixjQUFjLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLEdBQUcsR0FBRyxTQUFTLENBQUM7SUFDaEIsTUFBTSxHQUFHLFNBQVMsQ0FBQztJQUNuQixNQUFNLEdBQUcsU0FBUyxDQUFDO0lBQ25CLEVBQUUsR0FBRyxTQUFTLENBQUM7SUFDZixFQUFFLEdBQUcsU0FBUyxDQUFDO0FBQ25CLENBQUM7QUFFRCxTQUFTLFdBQVcsQ0FBQyxLQUFpQjtJQUNsQyxJQUFHLENBQUMsY0FBYyxFQUFDO1FBQ2YsT0FBTztLQUNWO0lBQ0QsSUFBRyxLQUFLLENBQUMsYUFBYSxJQUFJLEdBQUcsRUFBQztRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLGdHQUFnRyxDQUFDLENBQUM7S0FDakg7SUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxFQUFFLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ25GLEVBQUUsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO0lBQ25CLEVBQUUsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO0lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0lBQ3ZDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO0lBQ25GLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO0lBQ2pGLE1BQU0sR0FBRyxFQUFFLENBQUM7SUFDWixNQUFNLEdBQUcsRUFBRSxDQUFDO0FBQ2hCLENBQUM7QUFFRCxTQUFnQixhQUFhLENBQUMsRUFBZTtJQUN6QyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQzlDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDMUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQztBQUNsRCxDQUFDO0FBSkQsc0NBSUM7QUFFRCxTQUFnQixlQUFlLENBQUMsRUFBZTtJQUMzQyxFQUFFLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ2pELEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDN0MsRUFBRSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQztBQUNyRCxDQUFDO0FBSkQsMENBSUM7Ozs7OztBQ3pERDs7R0FFRztBQUNIO0lBT0ksbUJBQ0ksTUFBYyxFQUNkLE1BQWMsRUFDZCxRQUFnQixFQUNoQixRQUFnQixFQUNoQixLQUFhLEVBQ2IsS0FBYTtRQUViLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7SUFDTCxnQkFBQztBQUFELENBdEJBLEFBc0JDLElBQUE7QUF0QlksOEJBQVM7QUF3QnRCO0lBR0ksb0JBQVksQ0FBUyxFQUFFLENBQVM7UUFDNUIsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDWCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNmLENBQUM7SUFDTCxpQkFBQztBQUFELENBUEEsQUFPQyxJQUFBO0FBUFksZ0NBQVU7Ozs7O0FDM0J2Qix1Q0FBc0Q7QUFFdEQsSUFBRyxNQUFNLEVBQUM7SUFDQSxNQUFPLENBQUMsR0FBRyxHQUFHO1FBQ2hCLFFBQVEscUJBQUE7UUFDUixjQUFjLDJCQUFBO0tBQ2pCLENBQUE7Q0FDSjs7Ozs7O0FDTEQsSUFBWSxLQUdYO0FBSEQsV0FBWSxLQUFLO0lBQ2IsaUNBQUksQ0FBQTtJQUNKLHVDQUFPLENBQUE7QUFDWCxDQUFDLEVBSFcsS0FBSyxHQUFMLGFBQUssS0FBTCxhQUFLLFFBR2hCOzs7Ozs7QUNGRDtJQUVJO0lBQWMsQ0FBQztJQUNmLHlCQUFVLEdBQVYsVUFBVyxLQUFVO1FBQ2pCLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7WUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM5RDtJQUNMLENBQUM7SUFDRCxxQkFBTSxHQUFOLFVBQU8sR0FBa0IsRUFBRSxTQUFvQjtRQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDL0IsSUFBSSxLQUFLLEdBQUcsNEJBQTRCLENBQUM7UUFDekMsSUFBSSxDQUFDLE9BQU8sR0FBa0IsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsU0FBUyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQztRQUM5RCxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDeEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxtQkFBSSxHQUFKLFVBQUssRUFBYTtRQUNkLElBQUksRUFBRSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxNQUFNLEdBQUMsRUFBRSxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUM7U0FDOUU7YUFBTTtZQUNILElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLFFBQVEsR0FBQyxFQUFFLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQztTQUM5RTtRQUVELElBQUksRUFBRSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxNQUFNLEdBQUMsRUFBRSxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUM7U0FDL0U7YUFBTTtZQUNILElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLFFBQVEsR0FBQyxFQUFFLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQztTQUMvRTtJQUNMLENBQUM7SUFDRCx3QkFBUyxHQUFULFVBQVUsS0FBaUI7UUFDdkIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCwwQkFBVyxHQUFYLFVBQVksS0FBaUI7UUFDekIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCx5QkFBVSxHQUFWLFVBQVcsS0FBaUI7UUFDeEIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCx1QkFBUSxHQUFSLFVBQVMsS0FBaUI7UUFDdEIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCx5QkFBVSxHQUFWLFVBQVcsS0FBaUI7UUFDeEIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCxzQkFBSSxxQkFBRzthQUFQO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMxQyxDQUFDO2FBVUQsVUFBUSxLQUFhO1lBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbEQsQ0FBQzs7O09BWkE7SUFDRCxzQkFBSSxzQkFBSTthQUFSO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMxQyxDQUFDO2FBVUQsVUFBUyxLQUFhO1lBQ2xCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbEQsQ0FBQzs7O09BWkE7SUFDRCxzQkFBSSx1QkFBSzthQUFUO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM5QyxDQUFDO2FBYUQsVUFBVSxLQUFhO1lBQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDdEQsQ0FBQzs7O09BZkE7SUFDRCxzQkFBSSx3QkFBTTthQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMvQyxDQUFDO2FBT0QsVUFBVyxLQUFhO1lBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDdkQsQ0FBQzs7O09BVEE7SUFhRCwyQkFBWSxHQUFaLFVBQWEsSUFBWSxFQUFFLEtBQWE7UUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ0wsV0FBQztBQUFELENBL0VBLEFBK0VDLElBQUE7QUEvRVksb0JBQUkiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCJpbXBvcnQgeyBTSEFQRSwgU2hhcGUgfSBmcm9tIFwiLi9zaGFwZS5pbnRlcmZhY2VcIjtcbmltcG9ydCB7IFJlY3QgfSBmcm9tIFwiLi9zaGFwZXMvcmVjdFwiO1xuaW1wb3J0IHsgRHJhd0V2ZW50IH0gZnJvbSBcIi4vZXZlbnRcIjtcbmltcG9ydCB7IG1ha2VEcmFnZ2FibGUsIG1ha2VVbmRyYWdnYWJsZSB9IGZyb20gJy4vZHJhZ2dhYmxlJztcbi8qKlxuICogQWN0aXZpdHkgbmVlZHMgYSBjb250YWluZXIgYW5kIHRoZSBzaGFwZSBpbnRlbmRlZCB0byBkcmF3XG4gKiBBY3Rpdml0eSB3aWxsIGFkZCBhcHQgZXZlbnQgbGlzdGVuZXJzIHRvIHRoZSBjb250YWluZXJcbiAqIEFjdGl2aXR5IHdpbGwgY3JlYXRlIFBhaW50RXZlbnQgb2JqZWN0XG4gKiBJdCB3aWxsIHBhc3MgdGhlIFBhaW50RXZlbnQgb2JqZWN0IHRvIFxuICovXG5lbnVtIFN0YXRlIHtcbiAgICBSRUFEWSxcbiAgICBTVEFSVEVELFxuICAgIEZJTklTSEVEXG59O1xuXG5leHBvcnQgY2xhc3MgQWN0aXZpdHkge1xuICAgIGNvbnRhaW5lcjogSFRNTEVsZW1lbnQ7XG4gICAgc2hhcGVUeXBlOiBTSEFQRTtcbiAgICBzaGFwZTogU2hhcGU7XG4gICAgc3ZnOiBTVkdTVkdFbGVtZW50O1xuICAgIGRpdjogSFRNTEVsZW1lbnQ7XG4gICAgY29uZmlnOiBBY3Rpdml0eUNvbmZpZztcbiAgICBhY3Rpdml0eVN0YXRlOiBTdGF0ZTtcbiAgICBwcml2YXRlIHN0YXJ0WDogbnVtYmVyO1xuICAgIHByaXZhdGUgc3RhcnRZOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBtaW5YOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBtaW5ZOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBtYXhYOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBtYXhZOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBwcmV2WDogbnVtYmVyO1xuICAgIHByaXZhdGUgcHJldlk6IG51bWJlcjtcbiAgICBwcml2YXRlIHRvcEhhbmRsZTogSFRNTEVsZW1lbnQ7XG4gICAgcHJpdmF0ZSByaWdodEhhbmRsZTogSFRNTEVsZW1lbnQ7XG4gICAgcHJpdmF0ZSBsZWZ0SGFuZGxlOiBIVE1MRWxlbWVudDtcbiAgICBwcml2YXRlIGJvdHRvbUhhbmRsZTogSFRNTEVsZW1lbnQ7XG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyOiBIVE1MRWxlbWVudCwgc2hhcGVUeXBlOiBTSEFQRSwgY29uZmlnPzogQWN0aXZpdHlDb25maWcpIHtcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXI7XG4gICAgICAgIHRoaXMuc2hhcGVUeXBlID0gc2hhcGVUeXBlO1xuICAgICAgICBpZiAodGhpcy5zaGFwZVR5cGUgPT0gU0hBUEUuUkVDVCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJbQWN0aXZpdHldIGNvbnN0cnVjdGVkIHJlY3Qgc2hhcGVcIilcbiAgICAgICAgICAgIHRoaXMuc2hhcGUgPSBuZXcgUmVjdCgpO1xuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5zaGFwZSlcbiAgICAgICAgfVxuICAgICAgICBpZiAoY29uZmlnKSB7XG4gICAgICAgICAgICB0aGlzLmNvbmZpZyA9IGNvbmZpZztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuY29uZmlnID0gbmV3IEFjdGl2aXR5Q29uZmlnKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5hY3Rpdml0eVN0YXRlID0gU3RhdGUuUkVBRFk7XG4gICAgfVxuICAgIHJ1bigpIHtcbiAgICAgICAgdGhpcy5jcmVhdGVDaGlsZHJlbigpO1xuICAgICAgICB0aGlzLmF0dGFjaExpc3RlbmVyc1RvU3ZnKCk7XG4gICAgfVxuICAgIGNyZWF0ZUNoaWxkcmVuKCkge1xuICAgICAgICB0aGlzLnN2ZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUyhcImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCIsIFwic3ZnXCIpO1xuICAgICAgICB0aGlzLnN2Zy5zZXRBdHRyaWJ1dGUoJ3dpZHRoJywgdGhpcy5jb250YWluZXIuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkud2lkdGggKyBcIlwiKTtcbiAgICAgICAgdGhpcy5zdmcuc2V0QXR0cmlidXRlKCdoZWlnaHQnLCB0aGlzLmNvbnRhaW5lci5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5oZWlnaHQgKyBcIlwiKTtcbiAgICAgICAgdGhpcy5zdmcuc2V0QXR0cmlidXRlTlMoXCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3htbG5zL1wiLCBcInhtbG5zOnhsaW5rXCIsIFwiaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGlua1wiKTtcbiAgICAgICAgdGhpcy5kaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgICAgICB0aGlzLmRpdi5zdHlsZS53aWR0aCA9IHRoaXMuY29udGFpbmVyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoICsgXCJweFwiO1xuICAgICAgICB0aGlzLmRpdi5zdHlsZS5oZWlnaHQgPSB0aGlzLmNvbnRhaW5lci5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5oZWlnaHQgKyBcInB4XCI7XG4gICAgICAgIGNvbnNvbGUubG9nKFwicG9zaXRpb24gaXNcIiwgdGhpcy5jb250YWluZXIuc3R5bGUucG9zaXRpb24pXG4gICAgICAgIGlmKCF0aGlzLmNvbnRhaW5lci5zdHlsZS5wb3NpdGlvbiB8fCB0aGlzLmNvbnRhaW5lci5zdHlsZS5wb3NpdGlvbiA9PSBcInN0YXRpY1wiKXtcbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLnN0eWxlLnBvc2l0aW9uID0gXCJyZWxhdGl2ZVwiO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZGl2LnN0eWxlLnBvc2l0aW9uID0gXCJhYnNvbHV0ZVwiO1xuICAgICAgICB0aGlzLmNvbnRhaW5lci5hcHBlbmRDaGlsZCh0aGlzLmRpdik7XG4gICAgICAgIHRoaXMuZGl2LmFwcGVuZENoaWxkKHRoaXMuc3ZnKTtcbiAgICAgICAgdGhpcy5kaXYuY2xhc3NMaXN0LmFkZChcImNydXhkcmF3ZWxlbWVudFwiKTtcbiAgICAgICAgdGhpcy5kaXYuY2xhc3NMaXN0LmFkZChcImNydXhzdmdjb250YWluZXJcIik7XG4gICAgICAgIHRoaXMuc3ZnLmNsYXNzTGlzdC5hZGQoXCJjcnV4ZHJhd2VsZW1lbnRcIik7XG4gICAgICAgIHRoaXMuc3ZnLmNsYXNzTGlzdC5hZGQoXCJjcnV4c3ZnXCIpO1xuICAgICAgICAoPGFueT50aGlzLmRpdikuZ2V0QWN0aXZpdHkgPSAoKT0+e1xuICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgIH1cbiAgICAgICAgKDxhbnk+dGhpcy5zdmcpLmdldEFjdGl2aXR5ID0gKCk9PntcbiAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICB9XG4gICAgfVxuICAgIGF0dGFjaExpc3RlbmVyc1RvU3ZnKCkge1xuICAgICAgICB0aGlzLnN2Zy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIHRoaXMuc3RhcnREcmF3aW5nLmJpbmQodGhpcykpO1xuICAgICAgICB0aGlzLnN2Zy5hZGRFdmVudExpc3RlbmVyKFwibW91c2V1cFwiLCB0aGlzLnN0b3BEcmF3aW5nLmJpbmQodGhpcykpO1xuICAgICAgICB0aGlzLnN2Zy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIHRoaXMudXBkYXRlRHJhd2luZy5iaW5kKHRoaXMpKTtcbiAgICB9XG4gICAgYXR0YWNoTGlzdGVuZXJzVG9XaW5kb3coKSB7XG5cbiAgICB9XG4gICAgc3RhcnREcmF3aW5nKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgICAgIGlmICh0aGlzLmFjdGl2aXR5U3RhdGUgIT0gU3RhdGUuUkVBRFkpIHtcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcImF0dGVtcHRlZCB0byBzdGFydCBhbiBhY3Rpdml0eSBhZnRlciBpdCBoYXMgYmVlbiBzdGFydGVkIG9yIGZpbmlzaGVkXCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKFwic3RhcnRpbmcgZHJhd2luZ1wiKTtcbiAgICAgICAgdGhpcy5hY3Rpdml0eVN0YXRlID0gU3RhdGUuU1RBUlRFRDtcbiAgICAgICAgdGhpcy5zdGFydFggPSBldmVudC5jbGllbnRYIC0gdGhpcy5zdmcuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdDtcbiAgICAgICAgdGhpcy5zdGFydFkgPSBldmVudC5jbGllbnRZIC0gdGhpcy5zdmcuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wO1xuICAgICAgICB0aGlzLnVwZGF0ZVByZXYoZXZlbnQpO1xuICAgICAgICB0aGlzLnVwZGF0ZU1pbk1heChldmVudCk7XG4gICAgICAgIGxldCBkcmF3RXZlbnQ6IERyYXdFdmVudCA9IG5ldyBEcmF3RXZlbnQoXG4gICAgICAgICAgICB0aGlzLnN0YXJ0WCxcbiAgICAgICAgICAgIHRoaXMuc3RhcnRZLFxuICAgICAgICAgICAgZXZlbnQuY2xpZW50WCAtIHRoaXMuc3ZnLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmxlZnQsXG4gICAgICAgICAgICBldmVudC5jbGllbnRZIC0gdGhpcy5zdmcuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wLFxuICAgICAgICAgICAgdGhpcy5wcmV2WCxcbiAgICAgICAgICAgIHRoaXMucHJldllcbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5zaGFwZS5jcmVhdGUodGhpcy5zdmcsIGRyYXdFdmVudCk7XG4gICAgICAgIHRoaXMuc2hhcGUuYXBwbHlTdHlsZSh0aGlzLmNvbmZpZy5zaGFwZUNTUyk7XG4gICAgfVxuICAgIHN0b3BEcmF3aW5nKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgICAgIC8vIEFub3RoZXIgYXBwcm9hY2ggY2FuIGJlIHRvIGF0dGFjaCBtb3VzZW1vdmUgaW5zaWRlIHN0YXJ0RHJhd2luZ1xuICAgICAgICBpZiAodGhpcy5hY3Rpdml0eVN0YXRlID09IFN0YXRlLlJFQURZKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuYWN0aXZpdHlTdGF0ZSA9PSBTdGF0ZS5GSU5JU0hFRCkge1xuICAgICAgICAgICAgY29uc29sZS53YXJuKFwiYXR0ZW1wdGluZyB0byBzdG9wIGFuIGFscmVhZHkgc3RvcHBlZCBhY3Rpdml0eVwiKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBjb25zb2xlLmxvZyhcInN0b3BpbmcgZHJhd2luZ1wiKTtcbiAgICAgICAgdGhpcy5hY3Rpdml0eVN0YXRlID0gU3RhdGUuRklOSVNIRUQ7XG4gICAgICAgIGlmICh0aGlzLmNvbmZpZy5zbmFwVG9TaGFwZSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJzbmFwcGluZyB0byBzaGFwZVwiLCB0aGlzLmNvbmZpZyk7XG4gICAgICAgICAgICBsZXQgZ2FwID0gdGhpcy5jb25maWcuaGFuZGxlR2FwO1xuICAgICAgICAgICAgbGV0IG5ld1ggPSBwYXJzZUludCh0aGlzLnNoYXBlLmxlZnQucmVwbGFjZShcInB4XCIsIFwiXCIpKSAtIGdhcDtcbiAgICAgICAgICAgIGxldCBuZXdZID0gcGFyc2VJbnQodGhpcy5zaGFwZS50b3AucmVwbGFjZShcInB4XCIsIFwiXCIpKSAtIGdhcDtcbiAgICAgICAgICAgIGxldCBuZXdXaWR0aCA9IHBhcnNlSW50KHRoaXMuc2hhcGUud2lkdGgucmVwbGFjZShcInB4XCIsIFwiXCIpKSArIDIgKiBnYXAgKyBcInB4XCI7XG4gICAgICAgICAgICBsZXQgbmV3SGVpZ2h0ID0gcGFyc2VJbnQodGhpcy5zaGFwZS5oZWlnaHQucmVwbGFjZShcInB4XCIsIFwiXCIpKSArIDIgKiBnYXAgKyBcInB4XCI7XG4gICAgICAgICAgICB0aGlzLmRpdi5zdHlsZS50b3AgPSBuZXdZICsgXCJweFwiO1xuICAgICAgICAgICAgdGhpcy5kaXYuc3R5bGUubGVmdCA9IG5ld1ggKyBcInB4XCI7XG4gICAgICAgICAgICB0aGlzLmRpdi5zdHlsZS5oZWlnaHQgPSBuZXdIZWlnaHQ7XG4gICAgICAgICAgICB0aGlzLmRpdi5zdHlsZS53aWR0aCA9IG5ld1dpZHRoO1xuICAgICAgICAgICAgdGhpcy5kaXYuc3R5bGUuZGlzcGxheSA9IFwiaW5saW5lLWJsb2NrXCI7XG4gICAgICAgICAgICB0aGlzLnN2Zy5zdHlsZS5wb3NpdGlvbiA9IFwicmVsYXRpdmVcIjtcbiAgICAgICAgICAgIHRoaXMuc3ZnLnN0eWxlLmxlZnQgPSBnYXArXCJweFwiO1xuICAgICAgICAgICAgdGhpcy5zdmcuc3R5bGUudG9wID0gZ2FwICsgXCJweFwiO1xuICAgICAgICAgICAgdGhpcy5zdmcuc2V0QXR0cmlidXRlKCd3aWR0aCcsIHRoaXMuc2hhcGUud2lkdGgpO1xuICAgICAgICAgICAgdGhpcy5zdmcuc2V0QXR0cmlidXRlKCdoZWlnaHQnLCB0aGlzLnNoYXBlLmhlaWdodCk7XG4gICAgICAgICAgICBsZXQgbmV3UmVjdFggPSAwO1xuICAgICAgICAgICAgbGV0IG5ld1JlY3RZID0gMDtcbiAgICAgICAgICAgIHRoaXMuc2hhcGUubGVmdCA9IG5ld1JlY3RYICsgXCJcIjtcbiAgICAgICAgICAgIHRoaXMuc2hhcGUudG9wID0gbmV3UmVjdFkgKyBcIlwiO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJuZXcgcmVjdCBhdHRyaWJ1dGVzXCIsIG5ld1JlY3RYLCBuZXdSZWN0WSk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gcmVtb3ZlIGFsbCBldmVudCBsaXN0ZW5lcnMgZnJvbSBzdmdcbiAgICAgICAgbGV0IGNsb25lU3ZnID0gPFNWR1NWR0VsZW1lbnQ+dGhpcy5zdmcuY2xvbmVOb2RlKHRydWUpO1xuICAgICAgICB0aGlzLnN2Zy5wYXJlbnROb2RlLnJlcGxhY2VDaGlsZChjbG9uZVN2ZywgdGhpcy5zdmcpO1xuICAgICAgICB0aGlzLnN2ZyA9IGNsb25lU3ZnO1xuICAgICAgICBpZih0aGlzLmNvbmZpZy5zZWxlY3RhYmxlKXtcbiAgICAgICAgICAgIHRoaXMuc3ZnLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWRvd25cIiwgdGhpcy5vblNWR1NlbGVjdC5iaW5kKHRoaXMpKTtcbiAgICAgICAgfVxuICAgICAgICBsZXQgY0V2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KFwiY3J1eGFjdGl2aXR5Y29tcGxldGVcIiwge2RldGFpbDoge2FjdGl2aXR5OiB0aGlzfX0pO1xuICAgICAgICB3aW5kb3cuZGlzcGF0Y2hFdmVudChjRXZlbnQpO1xuICAgIH1cbiAgICB1cGRhdGVEcmF3aW5nKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgICAgIGlmICh0aGlzLmFjdGl2aXR5U3RhdGUgPT0gU3RhdGUuUkVBRFkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5hY3Rpdml0eVN0YXRlID09IFN0YXRlLkZJTklTSEVEKSB7XG4gICAgICAgICAgICBjb25zb2xlLndhcm4oXCJhdHRlbXB0ZWQgdG8gdXBkYXRlIGRyYXdpbmcgYWZ0ZXIgdGhlIGFjdGl2aXR5IGhhcyBmaW5pc2hlZFwiKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBjb25zb2xlLmxvZyhcInVwZGF0ZSBkcmF3aW5nXCIsIHRoaXMuYWN0aXZpdHlTdGF0ZSwgU3RhdGUuUkVBRFksIFN0YXRlLlNUQVJURUQsIFN0YXRlLkZJTklTSEVEKVxuICAgICAgICBsZXQgZHJhd0V2ZW50OiBEcmF3RXZlbnQgPSBuZXcgRHJhd0V2ZW50KFxuICAgICAgICAgICAgdGhpcy5zdGFydFgsXG4gICAgICAgICAgICB0aGlzLnN0YXJ0WSxcbiAgICAgICAgICAgIGV2ZW50LmNsaWVudFggLSB0aGlzLnN2Zy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0LFxuICAgICAgICAgICAgZXZlbnQuY2xpZW50WSAtIHRoaXMuc3ZnLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcCxcbiAgICAgICAgICAgIHRoaXMucHJldlgsXG4gICAgICAgICAgICB0aGlzLnByZXZZXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuc2hhcGUuZHJhdyhkcmF3RXZlbnQpO1xuICAgICAgICB0aGlzLnVwZGF0ZVByZXYoZXZlbnQpO1xuICAgIH1cbiAgICB1cGRhdGVNaW5NYXgoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcbiAgICAgICAgbGV0IG5ld1ggPSBldmVudC5jbGllbnRYLCBuZXdZID0gZXZlbnQuY2xpZW50WTtcbiAgICAgICAgaWYgKHRoaXMubWluWCA+IG5ld1ggfHwgISh0aGlzLm1pblgpKSB7XG4gICAgICAgICAgICB0aGlzLm1pblggPSBuZXdYO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLm1heFggPCBuZXdYIHx8ICEodGhpcy5tYXhYKSkge1xuICAgICAgICAgICAgdGhpcy5tYXhYID0gbmV3WDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5taW5ZID4gbmV3WSB8fCAhKHRoaXMubWluWSkpIHtcbiAgICAgICAgICAgIHRoaXMubWluWSA9IG5ld1k7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMubWF4WSA8IG5ld1kgfHwgISh0aGlzLm1heFkpKSB7XG4gICAgICAgICAgICB0aGlzLm1heFkgPSBuZXdZO1xuICAgICAgICB9XG4gICAgfVxuICAgIHVwZGF0ZVByZXYoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcbiAgICAgICAgdGhpcy5wcmV2WCA9IGV2ZW50LmNsaWVudFggLSB0aGlzLnN2Zy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0O1xuICAgICAgICB0aGlzLnByZXZZID0gZXZlbnQuY2xpZW50WSAtIHRoaXMuc3ZnLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcDtcbiAgICB9XG4gICAgb25TVkdTZWxlY3QoZXZlbnQ6IE1vdXNlRXZlbnQpe1xuICAgICAgICAvLyBjaGVjayBpZiB0aGUgdGFyZ2V0IGlzIGFscmVhZHkgc2VsZWN0ZWRcbiAgICAgICAgbGV0IHRhcmdldCA9IDxIVE1MRWxlbWVudD5ldmVudC50YXJnZXQ7XG4gICAgICAgIGlmKHRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoXCJjcnV4c2VsZWN0ZWRcIikpe1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2VsZWN0U2hhcGUoKTtcbiAgICAgICAgZnVuY3Rpb24gcmVtb3ZlU2VsZWN0aW9uKGV2ZW50OiBNb3VzZUV2ZW50KXtcbiAgICAgICAgICAgIGxldCB0YXJnZXQgPSA8SFRNTEVsZW1lbnQ+ZXZlbnQudGFyZ2V0O1xuICAgICAgICAgICAgaWYodGFyZ2V0ICE9IHRoaXMuc3ZnICYmIHRhcmdldCAhPSB0aGlzLmRpdiAmJiB0YXJnZXQgIT0gdGhpcy5zdmcuY2hpbGROb2Rlc1swXSl7XG4gICAgICAgICAgICAgICAgdGhpcy51bnNlbGVjdFNoYXBlKCk7XG4gICAgICAgICAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZWRvd25cIiwgcmVtb3ZlQ0IpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicmVtb3ZlZCBldmVudCBsaXN0ZW5lcnMgb24gd2luZG93XCIpO1xuICAgICAgICAgICAgICAgIGxldCBjRXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQoXCJjcnV4c3ZndW5zZWxlY3RlZFwiLCB7ZGV0YWlsOiB7YWN0aXZpdHk6IHRoaXN9fSk7XG4gICAgICAgICAgICAgICAgd2luZG93LmRpc3BhdGNoRXZlbnQoY0V2ZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBsZXQgcmVtb3ZlQ0IgPSByZW1vdmVTZWxlY3Rpb24uYmluZCh0aGlzKTtcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWRvd25cIiwgcmVtb3ZlQ0IpO1xuICAgICAgICBsZXQgY0V2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KFwiY3J1eHN2Z3NlbGVjdGVkXCIsIHtkZXRhaWw6IHthY3Rpdml0eTogdGhpc319KTtcbiAgICAgICAgd2luZG93LmRpc3BhdGNoRXZlbnQoY0V2ZW50KTtcbiAgICB9XG4gICAgc2VsZWN0U2hhcGUoKXtcbiAgICAgICAgLy8gQWRkIGFsbCBoYW5kbGVzIGFuZCBjb3JuZXJzIGhlcmUgb25seVxuICAgICAgICBjb25zb2xlLmxvZyhcIlNWRyBzZWxlY3RlZFwiKTtcbiAgICAgICAgKDxIVE1MRWxlbWVudD50aGlzLnN2Zy5jaGlsZE5vZGVzWzBdKS5jbGFzc0xpc3QuYWRkKFwiY3J1eHNlbGVjdGVkXCIpO1xuICAgICAgICB0aGlzLnN2Zy5jbGFzc0xpc3QuYWRkKFwiY3J1eHNlbGVjdGVkXCIpO1xuICAgICAgICB0aGlzLmRpdi5jbGFzc0xpc3QuYWRkKFwiY3J1eHNlbGVjdGVkXCIpO1xuICAgICAgICB0aGlzLnRvcEhhbmRsZSA9IGNyZWF0ZUhhbmRsZShcImhvcml6b250YWxcIiwgcGFyc2VJbnQodGhpcy5kaXYuc3R5bGUud2lkdGgucmVwbGFjZShcInB4XCIsIFwiXCIpKSAtIDIqdGhpcy5jb25maWcuaGFuZGxlR2FwLCB0aGlzLmNvbmZpZy5oYW5kbGVHYXAsIFwiXCIsIFwiXCIpO1xuICAgICAgICB0aGlzLnRvcEhhbmRsZS5zdHlsZS5wb3NpdGlvbiA9IFwiYWJzb2x1dGVcIjtcbiAgICAgICAgdGhpcy50b3BIYW5kbGUuc3R5bGUudG9wID0gXCIwXCI7XG4gICAgICAgIHRoaXMudG9wSGFuZGxlLnN0eWxlLmxlZnQgPSB0aGlzLmNvbmZpZy5oYW5kbGVHYXAgKyBcInB4XCI7XG4gICAgICAgIHRoaXMuZGl2LmFwcGVuZENoaWxkKHRoaXMudG9wSGFuZGxlKTtcbiAgICAgICAgdGhpcy5ib3R0b21IYW5kbGUgPSBjcmVhdGVIYW5kbGUoXCJob3Jpem9udGFsXCIsIHBhcnNlSW50KHRoaXMuZGl2LnN0eWxlLndpZHRoLnJlcGxhY2UoXCJweFwiLCBcIlwiKSkgLSAyKnRoaXMuY29uZmlnLmhhbmRsZUdhcCwgdGhpcy5jb25maWcuaGFuZGxlR2FwLCBcIlwiLCBcIlwiKTtcbiAgICAgICAgdGhpcy5ib3R0b21IYW5kbGUuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XG4gICAgICAgIHRoaXMuYm90dG9tSGFuZGxlLnN0eWxlLmJvdHRvbSA9IFwiMFwiO1xuICAgICAgICB0aGlzLmJvdHRvbUhhbmRsZS5zdHlsZS5sZWZ0ID0gdGhpcy5jb25maWcuaGFuZGxlR2FwICsgXCJweFwiO1xuICAgICAgICB0aGlzLmRpdi5hcHBlbmRDaGlsZCh0aGlzLmJvdHRvbUhhbmRsZSk7XG4gICAgICAgIHRoaXMubGVmdEhhbmRsZSA9IGNyZWF0ZUhhbmRsZShcInZlcnRpY2FsXCIsIHRoaXMuY29uZmlnLmhhbmRsZUdhcCwgcGFyc2VJbnQodGhpcy5kaXYuc3R5bGUuaGVpZ2h0LnJlcGxhY2UoXCJweFwiLCBcIlwiKSkgLSAyKnRoaXMuY29uZmlnLmhhbmRsZUdhcCwgXCJcIiwgXCJcIik7XG4gICAgICAgIHRoaXMubGVmdEhhbmRsZS5zdHlsZS5wb3NpdGlvbiA9IFwiYWJzb2x1dGVcIjtcbiAgICAgICAgdGhpcy5sZWZ0SGFuZGxlLnN0eWxlLnRvcCA9IHRoaXMuY29uZmlnLmhhbmRsZUdhcCArIFwicHhcIjtcbiAgICAgICAgdGhpcy5sZWZ0SGFuZGxlLnN0eWxlLmxlZnQgPSBcIjBcIjtcbiAgICAgICAgdGhpcy5kaXYuYXBwZW5kQ2hpbGQodGhpcy5sZWZ0SGFuZGxlKTtcbiAgICAgICAgdGhpcy5yaWdodEhhbmRsZSA9IGNyZWF0ZUhhbmRsZShcInZlcnRpY2FsXCIsIHRoaXMuY29uZmlnLmhhbmRsZUdhcCwgcGFyc2VJbnQodGhpcy5kaXYuc3R5bGUuaGVpZ2h0LnJlcGxhY2UoXCJweFwiLCBcIlwiKSkgLSAyKnRoaXMuY29uZmlnLmhhbmRsZUdhcCwgXCJcIiwgXCJcIik7XG4gICAgICAgIHRoaXMucmlnaHRIYW5kbGUuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XG4gICAgICAgIHRoaXMucmlnaHRIYW5kbGUuc3R5bGUudG9wID0gdGhpcy5jb25maWcuaGFuZGxlR2FwICsgXCJweFwiO1xuICAgICAgICB0aGlzLnJpZ2h0SGFuZGxlLnN0eWxlLnJpZ2h0ID0gXCIwXCI7XG4gICAgICAgIHRoaXMuZGl2LmFwcGVuZENoaWxkKHRoaXMucmlnaHRIYW5kbGUpO1xuICAgICAgICBtYWtlRHJhZ2dhYmxlKHRoaXMuZGl2KTtcbiAgICB9XG4gICAgdW5zZWxlY3RTaGFwZSgpe1xuICAgICAgICAvLyBSZW1vdmUgYWxsIGhhbmRsZXMgYW5kIGNvcm5lcnMgaGVyZSBvbmx5XG4gICAgICAgIGNvbnNvbGUubG9nKFwiU1ZHIHVuc2VsZWN0ZWRcIik7XG4gICAgICAgICg8SFRNTEVsZW1lbnQ+dGhpcy5zdmcuY2hpbGROb2Rlc1swXSkuY2xhc3NMaXN0LnJlbW92ZShcImNydXhzZWxlY3RlZFwiKTtcbiAgICAgICAgdGhpcy5zdmcuY2xhc3NMaXN0LnJlbW92ZShcImNydXhzZWxlY3RlZFwiKTtcbiAgICAgICAgdGhpcy5kaXYuY2xhc3NMaXN0LnJlbW92ZShcImNydXhzZWxlY3RlZFwiKTtcbiAgICAgICAgdGhpcy5sZWZ0SGFuZGxlLnBhcmVudEVsZW1lbnQucmVtb3ZlQ2hpbGQodGhpcy5sZWZ0SGFuZGxlKTtcbiAgICAgICAgdGhpcy5sZWZ0SGFuZGxlID0gdW5kZWZpbmVkO1xuICAgICAgICB0aGlzLnJpZ2h0SGFuZGxlLnBhcmVudEVsZW1lbnQucmVtb3ZlQ2hpbGQodGhpcy5yaWdodEhhbmRsZSk7XG4gICAgICAgIHRoaXMucmlnaHRIYW5kbGUgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMudG9wSGFuZGxlLnBhcmVudEVsZW1lbnQucmVtb3ZlQ2hpbGQodGhpcy50b3BIYW5kbGUpO1xuICAgICAgICB0aGlzLnRvcEhhbmRsZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgdGhpcy5ib3R0b21IYW5kbGUucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZCh0aGlzLmJvdHRvbUhhbmRsZSk7XG4gICAgICAgIHRoaXMuYm90dG9tSGFuZGxlID0gdW5kZWZpbmVkO1xuICAgICAgICBtYWtlVW5kcmFnZ2FibGUodGhpcy5kaXYpO1xuICAgIH1cbiAgICBtYWtlUmVzaXphYmxlKCl7XG5cbiAgICB9XG4gICAgbWFrZVVucmVzaXphYmxlKCl7XG5cbiAgICB9XG4gICAgZ2V0UG9zaXRpb24oKTogUG9zaXRpb257XG4gICAgICAgIGxldCBwb3NpdGlvbjogUG9zaXRpb24gPSBuZXcgUG9zaXRpb24oKTtcbiAgICAgICAgcG9zaXRpb24udG9wID0gcGFyc2VJbnQodGhpcy5kaXYuc3R5bGUudG9wLnJlcGxhY2UoXCJweFwiLCBcIlwiKSk7XG4gICAgICAgIHBvc2l0aW9uLmxlZnQgPSBwYXJzZUludCh0aGlzLmRpdi5zdHlsZS5sZWZ0LnJlcGxhY2UoXCJweFwiLCBcIlwiKSk7XG4gICAgICAgIHBvc2l0aW9uLmhlaWdodCA9IHBhcnNlSW50KHRoaXMuZGl2LnN0eWxlLmhlaWdodC5yZXBsYWNlKFwicHhcIiwgXCJcIikpO1xuICAgICAgICBwb3NpdGlvbi53aWR0aCA9IHBhcnNlSW50KHRoaXMuZGl2LnN0eWxlLndpZHRoLnJlcGxhY2UoXCJweFwiLCBcIlwiKSk7XG4gICAgICAgIHJldHVybiBwb3NpdGlvbjtcbiAgICB9XG4gICAgZ2V0Q2xpZW50UG9zaXRpb24oKTogUG9zaXRpb257XG4gICAgICAgIGxldCBwb3NpdGlvbjogUG9zaXRpb24gPSBuZXcgUG9zaXRpb24oKTtcbiAgICAgICAgbGV0IGRpdlBvcyA9IHRoaXMuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgcG9zaXRpb24uaGVpZ2h0ID0gcGFyc2VJbnQodGhpcy5zdmcuZ2V0QXR0cmlidXRlTlMobnVsbCwgXCJoZWlnaHRcIikucmVwbGFjZShcInB4XCIsIFwiXCIpKTtcbiAgICAgICAgcG9zaXRpb24ud2lkdGggPSBwYXJzZUludCh0aGlzLnN2Zy5nZXRBdHRyaWJ1dGVOUyhudWxsLCBcIndpZHRoXCIpLnJlcGxhY2UoXCJweFwiLCBcIlwiKSk7XG4gICAgICAgIHBvc2l0aW9uLnRvcCA9IGRpdlBvcy50b3AgKyBwYXJzZUludCh0aGlzLnN2Zy5zdHlsZS5sZWZ0LnJlcGxhY2UoXCJweFwiLCBcIlwiKSk7XG4gICAgICAgIHBvc2l0aW9uLmxlZnQgPSBkaXZQb3MubGVmdCArIHBhcnNlSW50KHRoaXMuc3ZnLnN0eWxlLmxlZnQucmVwbGFjZShcInB4XCIsIFwiXCIpKTtcbiAgICAgICAgcmV0dXJuIHBvc2l0aW9uO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEFjdGl2aXR5Q29uZmlnIHtcbiAgICBoYW5kbGVHYXA6IG51bWJlcjtcbiAgICBoYW5kbGVDb2xvcjogc3RyaW5nO1xuICAgIGhhbmRsZVN0cm9rZTogc3RyaW5nO1xuICAgIGNvcm5lckNvbG9yOiBzdHJpbmc7XG4gICAgc2hhcGVDU1M6IE9iamVjdDtcbiAgICBzZWxlY3RhYmxlOiBib29sZWFuO1xuICAgIHJlc2l6YWJsZTogYm9vbGVhbjtcbiAgICBkcmFnZ2FibGU6IGJvb2xlYW47XG4gICAgc25hcFRvU2hhcGU6IGJvb2xlYW47XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHNoYXBlQ3NzPzogT2JqZWN0LFxuICAgICAgICBoYW5kbGVHYXA/OiBudW1iZXIsXG4gICAgICAgIGhhbmRsZUNvbG9yPzogc3RyaW5nLFxuICAgICAgICBoYW5kbGVTdHJva2U/OiBzdHJpbmcsXG4gICAgICAgIGNvcm5lckNvbG9yPzogc3RyaW5nLFxuICAgICAgICBzZWxlY3RhYmxlPzogYm9vbGVhbixcbiAgICAgICAgcmVzaXplYWJsZT86IGJvb2xlYW4sXG4gICAgICAgIGRyYWdnYWJsZT86IGJvb2xlYW4sXG4gICAgICAgIHNuYXBUb1NoYXBlPzogYm9vbGVhblxuICAgICkge1xuICAgICAgICB0aGlzLnNoYXBlQ1NTID0gc2hhcGVDc3MgPyBzaGFwZUNzcyA6IHsgZmlsbDogXCJyZ2JhKDE5NiwgMTk2LCAxOTYsIDUwKVwiIH07XG4gICAgICAgIHRoaXMuaGFuZGxlR2FwID0gaGFuZGxlR2FwID8gaGFuZGxlR2FwIDogMTA7XG4gICAgICAgIHRoaXMuaGFuZGxlQ29sb3IgPSBoYW5kbGVDb2xvciA/IGhhbmRsZUNvbG9yIDogXCJyZ2JhKDAsIDAsIDI1NSwgNTApXCI7XG4gICAgICAgIHRoaXMuaGFuZGxlU3Ryb2tlID0gaGFuZGxlU3Ryb2tlID8gaGFuZGxlU3Ryb2tlIDogXCJkYXNoZWRcIjtcbiAgICAgICAgdGhpcy5jb3JuZXJDb2xvciA9IGNvcm5lckNvbG9yID8gY29ybmVyQ29sb3IgOiBcInJnYmEoMCwgMCwgMjU1LCAxMDApXCI7XG4gICAgICAgIHRoaXMuc2VsZWN0YWJsZSA9IHNlbGVjdGFibGUgPyBzZWxlY3RhYmxlIDogdHJ1ZTtcbiAgICAgICAgdGhpcy5yZXNpemFibGUgPSByZXNpemVhYmxlID8gcmVzaXplYWJsZSA6IHRydWU7XG4gICAgICAgIHRoaXMuZHJhZ2dhYmxlID0gZHJhZ2dhYmxlID8gZHJhZ2dhYmxlIDogdHJ1ZTtcbiAgICAgICAgdGhpcy5zbmFwVG9TaGFwZSA9IHNuYXBUb1NoYXBlID8gc25hcFRvU2hhcGUgOiB0cnVlO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gY3JlYXRlQ29ybmVyKHNpemU6IG51bWJlciwgY29sb3I6IHN0cmluZyk6IEhUTUxFbGVtZW50e1xuICAgIHJldHVybjtcbn1cblxuZnVuY3Rpb24gY3JlYXRlSGFuZGxlKGRpcmVjdGlvbjogc3RyaW5nLCBsZW5ndGg6IG51bWJlciwgaGVpZ2h0OiBudW1iZXIsIGNvbG9yOiBzdHJpbmcsIHN0cm9rZTogc3RyaW5nKTogSFRNTEVsZW1lbnR7XG4gICAgbGV0IGhhbmRsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIpO1xuICAgIGhhbmRsZS5jbGFzc0xpc3QuYWRkKFwiY3J1eGRyYXdlbGVtZW50XCIpO1xuICAgIGhhbmRsZS5jbGFzc0xpc3QuYWRkKFwiY3J1eGhhbmRsZVwiKTtcbiAgICBoYW5kbGUuc3R5bGUud2lkdGggPSBsZW5ndGggKyBcInB4XCI7XG4gICAgaGFuZGxlLnN0eWxlLmhlaWdodCA9IGhlaWdodCArIFwicHhcIjtcbiAgICBpZihkaXJlY3Rpb24gPT0gXCJob3Jpem9udGFsXCIpe1xuICAgICAgICBoYW5kbGUuc3R5bGUuYmFja2dyb3VuZCA9IFwicmVwZWF0aW5nLWxpbmVhci1ncmFkaWVudCh0byByaWdodCwgYmx1ZSAwLGJsdWUgMTBweCx0cmFuc3BhcmVudCAxMHB4LHRyYW5zcGFyZW50IDE1cHgpIGNlbnRlclwiO1xuICAgICAgICBoYW5kbGUuc3R5bGUuYmFja2dyb3VuZFNpemUgPSBcIjEwMCUgM3B4XCI7XG4gICAgICAgIGhhbmRsZS5zdHlsZS5iYWNrZ3JvdW5kUmVwZWF0ID0gXCJuby1yZXBlYXRcIjtcbiAgICB9XG4gICAgaWYoZGlyZWN0aW9uID09IFwidmVydGljYWxcIil7XG4gICAgICAgIGhhbmRsZS5zdHlsZS5iYWNrZ3JvdW5kID0gXCJyZXBlYXRpbmctbGluZWFyLWdyYWRpZW50KDBkZWcsYmx1ZSAwLGJsdWUgMTBweCx0cmFuc3BhcmVudCAxcHgsdHJhbnNwYXJlbnQgMTVweCkgcmVwZWF0LXkgY2VudGVyIHRvcFwiO1xuICAgICAgICBoYW5kbGUuc3R5bGUuYmFja2dyb3VuZFNpemUgPSBcIjNweCAxMDAlXCI7XG4gICAgfVxuICAgIHJldHVybiBoYW5kbGU7XG59XG5cbmV4cG9ydCBjbGFzcyBQb3NpdGlvbntcbiAgICB0b3A6IG51bWJlcjtcbiAgICBsZWZ0OiBudW1iZXI7XG4gICAgaGVpZ2h0OiBudW1iZXI7XG4gICAgd2lkdGg6IG51bWJlclxufSIsInZhciBpc01vdXNlUHJlc3NlZCA9IGZhbHNlO1xudmFyIGNFbDogSFRNTEVsZW1lbnQ7ICAgICAgICAgICAgICAgICAgICAvLyBjdXJyZW50IGVsZW1lbnQgYmVpbmcgZHJhZ2dlZFxudmFyIHN0YXJ0WDogbnVtYmVyLCBzdGFydFk6IG51bWJlcjsgICAgICAgICAvLyB4IGFuZCB5IGNvb3JkIGF0IHRoZSBzdGFydCBvZiBkcmFnIGV2ZW50XG52YXIgY1g6IG51bWJlciwgY1k6IG51bWJlcjsgICAgICAgICAgICAgICAgIC8vIGN1cnJlbnQgWCBhbmQgY3VycmVudCBZIGNvb3JkaW5hdGVcbmZ1bmN0aW9uIG9uTW91c2VEb3duKGV2ZW50OiBNb3VzZUV2ZW50KXtcbiAgICBjb25zb2xlLmxvZyhcImRyYWdnYWJsZS50czogb25Nb3VzZURvd24gY2FsbGVkXCIsIGV2ZW50LnRhcmdldCwgZXZlbnQuY3VycmVudFRhcmdldCk7XG4gICAgaXNNb3VzZVByZXNzZWQgPSB0cnVlO1xuICAgIGNFbCA9IDxIVE1MRWxlbWVudD5ldmVudC5jdXJyZW50VGFyZ2V0O1xuICAgIHN0YXJ0WCA9IGV2ZW50LmNsaWVudFg7XG4gICAgc3RhcnRZID0gZXZlbnQuY2xpZW50WTtcbiAgICBjb25zb2xlLmxvZyhcInN0YXJ0IHRvcCBhbmQgbGVmdFwiLCBjRWwuc3R5bGUudG9wLCBjRWwuc3R5bGUubGVmdCk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIG9uTW91c2VVcCk7XG59XG5cbmZ1bmN0aW9uIG9uTW91c2VVcChldmVudDogTW91c2VFdmVudCl7XG4gICAgaWYoIWlzTW91c2VQcmVzc2VkKXtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZyhcImRyYWdnYWJsZS50czogb25Nb3VzZVVwIGNhbGxlZFwiLCBldmVudC50YXJnZXQsIGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2V1cFwiLCB0aGlzKTtcbiAgICBsZXQgY0V2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KFwiY3J1eGRyYWdjb21wbGV0ZVwiLCB7ZGV0YWlsOiB7YWN0aXZpdHk6ICg8YW55PmNFbCkuZ2V0QWN0aXZpdHkoKX19KTtcbiAgICB3aW5kb3cuZGlzcGF0Y2hFdmVudChjRXZlbnQpO1xuICAgIGlzTW91c2VQcmVzc2VkID0gZmFsc2U7XG4gICAgY0VsID0gdW5kZWZpbmVkO1xuICAgIHN0YXJ0WCA9IHVuZGVmaW5lZDtcbiAgICBzdGFydFkgPSB1bmRlZmluZWQ7XG4gICAgY1ggPSB1bmRlZmluZWQ7XG4gICAgY1kgPSB1bmRlZmluZWQ7XG59XG5cbmZ1bmN0aW9uIG9uTW91c2VNb3ZlKGV2ZW50OiBNb3VzZUV2ZW50KXtcbiAgICBpZighaXNNb3VzZVByZXNzZWQpe1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmKGV2ZW50LmN1cnJlbnRUYXJnZXQgIT0gY0VsKXtcbiAgICAgICAgY29uc29sZS5sb2coXCJXQVJOOiBUaGlzIHdhcyBhbiBleHBlY3RlZCBlcnJvciBvY2N1cmVkIGR1ZSB0byBvbmUgZWxlbWVudCBvdmVybGFwcGluZyBvdmVyIG90aGVyIGR1cmluZyBkcmFnXCIpO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZyhcImRyYWdnYWJsZS50czogb25Nb3VzZU1vdmUgY2FsbGVkXCIsIGV2ZW50LnRhcmdldCwgZXZlbnQuY3VycmVudFRhcmdldCk7XG4gICAgY1ggPSBldmVudC5jbGllbnRYO1xuICAgIGNZID0gZXZlbnQuY2xpZW50WTtcbiAgICBjb25zb2xlLmxvZyhjRWwuc3R5bGUubGVmdCwgY1gsIHN0YXJ0WClcbiAgICBjRWwuc3R5bGUubGVmdCA9IHBhcnNlSW50KGNFbC5zdHlsZS5sZWZ0LnJlcGxhY2UoXCJweFwiLCBcIlwiKSkgKyAoY1ggLSBzdGFydFgpICsgXCJweFwiO1xuICAgIGNFbC5zdHlsZS50b3AgPSBwYXJzZUludChjRWwuc3R5bGUudG9wLnJlcGxhY2UoXCJweFwiLCBcIlwiKSkgKyAoY1kgLSBzdGFydFkpICsgXCJweFwiO1xuICAgIHN0YXJ0WCA9IGNYO1xuICAgIHN0YXJ0WSA9IGNZO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbWFrZURyYWdnYWJsZShlbDogSFRNTEVsZW1lbnQpe1xuICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWRvd25cIiwgb25Nb3VzZURvd24pO1xuICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIG9uTW91c2VVcCk7XG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbW92ZVwiLCBvbk1vdXNlTW92ZSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBtYWtlVW5kcmFnZ2FibGUoZWw6IEhUTUxFbGVtZW50KXtcbiAgICBlbC5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIG9uTW91c2VEb3duKTtcbiAgICBlbC5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2V1cFwiLCBvbk1vdXNlVXApO1xuICAgIGVsLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgb25Nb3VzZU1vdmUpO1xufSIsIi8qKlxuICogUGFpbnRFdmVudCB3aWxsIGJlIHJlcXVpcmVkIGFsbCBzaGFwZXMgdG8gZHJhdyB0aGVtc2VsdmVzXG4gKi9cbmV4cG9ydCBjbGFzcyBEcmF3RXZlbnQge1xuICAgIHN0YXJ0WDogbnVtYmVyO1xuICAgIHN0YXJ0WTogbnVtYmVyO1xuICAgIGN1cnJlbnRYOiBudW1iZXI7XG4gICAgY3VycmVudFk6IG51bWJlcjtcbiAgICBwcmV2WDogbnVtYmVyO1xuICAgIHByZXZZOiBudW1iZXI7XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHN0YXJ0WDogbnVtYmVyLFxuICAgICAgICBzdGFydFk6IG51bWJlcixcbiAgICAgICAgY3VycmVudFg6IG51bWJlcixcbiAgICAgICAgY3VycmVudFk6IG51bWJlcixcbiAgICAgICAgcHJldlg6IG51bWJlcixcbiAgICAgICAgcHJldlk6IG51bWJlclxuICAgICkge1xuICAgICAgICB0aGlzLnN0YXJ0WCA9IHN0YXJ0WDtcbiAgICAgICAgdGhpcy5zdGFydFkgPSBzdGFydFk7XG4gICAgICAgIHRoaXMuY3VycmVudFggPSBjdXJyZW50WDtcbiAgICAgICAgdGhpcy5jdXJyZW50WSA9IGN1cnJlbnRZO1xuICAgICAgICB0aGlzLnByZXZYID0gcHJldlg7XG4gICAgICAgIHRoaXMucHJldlkgPSBwcmV2WTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBEZWx0YUV2ZW50IHtcbiAgICB4OiBudW1iZXI7XG4gICAgeTogbnVtYmVyO1xuICAgIGNvbnN0cnVjdG9yKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMueCA9IHg7XG4gICAgICAgIHRoaXMueSA9IHk7XG4gICAgfVxufSIsImltcG9ydCB7IEFjdGl2aXR5LCBBY3Rpdml0eUNvbmZpZyB9IGZyb20gXCIuL2FjdGl2aXR5XCI7XG5cbmlmKHdpbmRvdyl7XG4gICAgKDxhbnk+d2luZG93KS5BcnQgPSB7XG4gICAgICAgIEFjdGl2aXR5LFxuICAgICAgICBBY3Rpdml0eUNvbmZpZ1xuICAgIH1cbn1cbiIsImltcG9ydCB7IERyYXdFdmVudCwgRGVsdGFFdmVudCB9IGZyb20gXCIuL2V2ZW50XCI7XG5cbmV4cG9ydCBlbnVtIFNIQVBFIHtcbiAgICBSRUNULFxuICAgIEVMTElQU0Vcbn1cblxuZXhwb3J0IGludGVyZmFjZSBTaGFwZSB7XG4gICAgY3JlYXRlKHN2ZzogU1ZHU1ZHRWxlbWVudCwgZXY6IERyYXdFdmVudCk6IFNWR1NWR0VsZW1lbnQ7XG4gICAgZHJhdyhldjogRHJhd0V2ZW50KTogdm9pZDtcbiAgICBtb3ZlRGVsdGEoZGVsdGE6IERlbHRhRXZlbnQpOiB2b2lkO1xuICAgIHJlc2l6ZVJpZ2h0KGRlbHRhOiBEZWx0YUV2ZW50KTogdm9pZDtcbiAgICByZXNpemVMZWZ0KGRlbHRhOiBEZWx0YUV2ZW50KTogdm9pZDtcbiAgICByZXNpemVVcChkZWx0YTogRGVsdGFFdmVudCk6IHZvaWQ7XG4gICAgcmVzaXplRG93bihkZWx0YTogRGVsdGFFdmVudCk6IHZvaWQ7XG4gICAgYXBwbHlTdHlsZShzdHlsZTogT2JqZWN0KTogdm9pZDtcbiAgICB0b3A6IHN0cmluZztcbiAgICBsZWZ0OiBzdHJpbmc7XG4gICAgd2lkdGg6IHN0cmluZztcbiAgICBoZWlnaHQ6IHN0cmluZztcbiAgICBzZXRBdHRyaWJ1dGUoYXR0cjogc3RyaW5nLCB2YWx1ZTogc3RyaW5nKTogdm9pZDtcbn0iLCJpbXBvcnQgeyBTaGFwZSB9IGZyb20gXCIuLi9zaGFwZS5pbnRlcmZhY2VcIjtcbmltcG9ydCB7IERyYXdFdmVudCwgRGVsdGFFdmVudCB9IGZyb20gXCIuLi9ldmVudFwiO1xuXG5leHBvcnQgY2xhc3MgUmVjdCBpbXBsZW1lbnRzIFNoYXBle1xuICAgIGVsZW1lbnQ6IFNWR1NWR0VsZW1lbnQ7XG4gICAgY29uc3RydWN0b3IoKXt9XG4gICAgYXBwbHlTdHlsZShzdHlsZTogYW55KTogdm9pZCB7XG4gICAgICAgIGxldCBrZXlzID0gT2JqZWN0LmtleXMoc3R5bGUpO1xuICAgICAgICBmb3IobGV0IGk9MDsgaSA8IGtleXMubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsIGtleXNbaV0sIHN0eWxlW2tleXNbaV1dKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBjcmVhdGUoc3ZnOiBTVkdTVkdFbGVtZW50LCBkcmF3RXZlbnQ6IERyYXdFdmVudCk6IFNWR1NWR0VsZW1lbnQge1xuICAgICAgICBjb25zb2xlLmxvZyhcImRyYXdSZWN0IGNhbGxlZFwiKTtcbiAgICAgICAgdmFyIHN2Z25zID0gXCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiO1xuICAgICAgICB0aGlzLmVsZW1lbnQgPSA8U1ZHU1ZHRWxlbWVudD5kb2N1bWVudC5jcmVhdGVFbGVtZW50TlMoc3ZnbnMsICdyZWN0Jyk7XG4gICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGVOUyhudWxsLCAneCcsIGRyYXdFdmVudC5zdGFydFggKyBcIlwiKTtcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsICd5JywgZHJhd0V2ZW50LnN0YXJ0WSArIFwiXCIpO1xuICAgICAgICBzdmcuYXBwZW5kQ2hpbGQodGhpcy5lbGVtZW50KTtcbiAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJjcnV4ZHJhd2VsZW1lbnRcIik7XG4gICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiY3J1eHNoYXBlXCIpO1xuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50O1xuICAgIH1cbiAgICBkcmF3KGV2OiBEcmF3RXZlbnQpOiB2b2lkIHtcbiAgICAgICAgaWYgKGV2LmN1cnJlbnRYIC0gZXYuc3RhcnRYID4gMCkge1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsICd4JywgZXYuc3RhcnRYK1wiXCIpO1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsICd3aWR0aCcsIGV2LmN1cnJlbnRYIC0gZXYuc3RhcnRYICsgXCJweFwiKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGVOUyhudWxsLCAneCcsIGV2LmN1cnJlbnRYK1wiXCIpO1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsICd3aWR0aCcsIGV2LnN0YXJ0WCAtIGV2LmN1cnJlbnRYICsgXCJweFwiKTtcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBpZiAoZXYuY3VycmVudFkgLSBldi5zdGFydFkgPiAwKSB7XG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlTlMobnVsbCwgJ3knLCBldi5zdGFydFkrXCJcIik7XG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2hlaWdodCcsIGV2LmN1cnJlbnRZIC0gZXYuc3RhcnRZICsgXCJweFwiKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGVOUyhudWxsLCAneScsIGV2LmN1cnJlbnRZK1wiXCIpO1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsICdoZWlnaHQnLCBldi5zdGFydFkgLSBldi5jdXJyZW50WSArIFwicHhcIik7XG4gICAgICAgIH1cbiAgICB9XG4gICAgbW92ZURlbHRhKGRlbHRhOiBEZWx0YUV2ZW50KTogdm9pZCB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIk1ldGhvZCBub3QgaW1wbGVtZW50ZWQuXCIpO1xuICAgIH1cbiAgICByZXNpemVSaWdodChkZWx0YTogRGVsdGFFdmVudCk6IHZvaWQge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJNZXRob2Qgbm90IGltcGxlbWVudGVkLlwiKTtcbiAgICB9XG4gICAgcmVzaXplTGVmdChkZWx0YTogRGVsdGFFdmVudCk6IHZvaWQge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJNZXRob2Qgbm90IGltcGxlbWVudGVkLlwiKTtcbiAgICB9XG4gICAgcmVzaXplVXAoZGVsdGE6IERlbHRhRXZlbnQpOiB2b2lkIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTWV0aG9kIG5vdCBpbXBsZW1lbnRlZC5cIik7XG4gICAgfVxuICAgIHJlc2l6ZURvd24oZGVsdGE6IERlbHRhRXZlbnQpOiB2b2lkIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTWV0aG9kIG5vdCBpbXBsZW1lbnRlZC5cIik7XG4gICAgfVxuICAgIGdldCB0b3AoKTogc3RyaW5ne1xuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcInlcIik7XG4gICAgfVxuICAgIGdldCBsZWZ0KCk6IHN0cmluZ3tcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJ4XCIpO1xuICAgIH1cbiAgICBnZXQgd2lkdGgoKTogc3RyaW5ne1xuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcIndpZHRoXCIpO1xuICAgIH1cbiAgICBnZXQgaGVpZ2h0KCk6IHN0cmluZ3tcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJoZWlnaHRcIik7XG4gICAgfVxuICAgIHNldCB0b3AodmFsdWU6IHN0cmluZyl7XG4gICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGVOUyhudWxsLCBcInlcIiwgdmFsdWUpO1xuICAgIH1cbiAgICBzZXQgbGVmdCh2YWx1ZTogc3RyaW5nKXtcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsIFwieFwiLCB2YWx1ZSk7XG4gICAgfVxuICAgIHNldCBoZWlnaHQodmFsdWU6IHN0cmluZyl7XG4gICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGVOUyhudWxsLCBcImhlaWdodFwiLCB2YWx1ZSk7XG4gICAgfVxuICAgIHNldCB3aWR0aCh2YWx1ZTogc3RyaW5nKXtcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsIFwid2lkdGhcIiwgdmFsdWUpO1xuICAgIH1cbiAgICBzZXRBdHRyaWJ1dGUoYXR0cjogc3RyaW5nLCB2YWx1ZTogc3RyaW5nKXtcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsIGF0dHIsIHZhbHVlKTtcbiAgICB9XG59Il19
